//
//  ProfileTableViewCell.m
//  ahaactioncenter
//
//  Created by Cary, Patrick on 11/25/16.
//  Copyright © 2016 AHA. All rights reserved.
//

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
