//
//  TESTAHANewsDetailsViewController.h
//  ahaactioncenter
//
//  Created by Cary, Patrick on 8/20/18.
//  Copyright © 2018 AHA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TESTAHANewsDetailsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *newsImage;
@property (strong, nonatomic) IBOutlet UILabel *newsTitle;
@property (strong, nonatomic) IBOutlet UITextView *newsDescription;

@property NSInteger tag;

@end
