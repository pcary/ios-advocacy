//
//  AHANewsViewController.m
//  ahaactioncenter
//
//  Created by Cary, Patrick on 3/6/19.
//  Copyright © 2019 AHA. All rights reserved.
//

#import "AHANewsViewController.h"

@interface AHANewsViewController ()

@end

@implementation AHANewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *optionsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(optionsButtonPressed:)];
    
    self.navigationItem.rightBarButtonItem = optionsButton;
    
    self.title = [self.articleDict objectForKey:@"topic"];
    self.articleTitle = [self.articleDict objectForKey:@"new_title"];
    
    NSString *imageString = [self.articleDict objectForKey:@"image"];
    
    if (imageString == nil) {
        [self.mainImageView hnk_setImageFromURL:[NSURL URLWithString:@"https://www.aha.org/sites/default/files/2018-03/AHANews_logo_2.jpg"]];
    } else {
        [self.mainImageView hnk_setImageFromURL:[NSURL URLWithString:imageString]];
    }
    
    self.descriptionLabel.text = [self stripHTML:[self.articleDict objectForKey:@"new_description"]];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    paragraphStyle.headIndent = 17;
    paragraphStyle.firstLineHeadIndent = 17;
    paragraphStyle.lineSpacing = 4;
    
    self.headlineLabel.attributedText = [[NSAttributedString alloc] initWithString:self.articleTitle attributes:@{NSParagraphStyleAttributeName: paragraphStyle}];
    
    self.dateLabel.attributedText = [[NSAttributedString alloc] initWithString:[self.articleDict objectForKey:@"date"] attributes:@{NSParagraphStyleAttributeName: paragraphStyle, NSForegroundColorAttributeName: [UIColor darkGrayColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Light" size:14]}];
    
    self.headlineLabel.numberOfLines = 2;
    self.headlineLabel.minimumScaleFactor = 0.5;
    self.headlineLabel.adjustsFontSizeToFitWidth = YES;
    
    self.headlineLabel.alpha = .95f;
    self.headlineLabel.layer.shadowOpacity = 1.0;
    self.headlineLabel.layer.shadowRadius = 0.0;
    self.headlineLabel.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.headlineLabel.layer.shadowOffset = CGSizeMake(0,0.5);

    NSLog(@"%@", self.articleDict);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [defaults setBool:YES forKey:self.articleTitle];
    [center postNotificationName:@"articleUpdates" object:nil];
}

-(NSString *) stripHTML:(NSString*)originalString {
    
    NSRange range;
    
    NSString *string = [originalString copy];
    
    while ((range = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        string = [string stringByReplacingCharactersInRange:range withString:@""];
    
    return string;
}

- (IBAction)optionsButtonPressed:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *share = [UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self shareArticle];
    }];
    
    NSString *actionTitle;
    if ([defaults boolForKey:self.articleTitle] == YES) {
        actionTitle = @"Mark as Unread";
    } else {
        actionTitle = @"Mark as Read";
    }
    
    UIAlertAction *markAs = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if ([defaults boolForKey:self.articleTitle] == YES) {
            [defaults setBool:NO forKey:self.articleTitle];
        } else {
            [defaults setBool:YES forKey:self.articleTitle];
        }
        [center postNotificationName:@"articleUpdates" object:nil];
        [defaults synchronize];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Cancel");
    }];
    
    [actionSheet addAction:share];
    [actionSheet addAction:markAs];
    [actionSheet addAction:cancel];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:actionSheet animated:YES completion:nil];
    });
}

-(void)shareArticle {
    NSString *string = [self.articleDict objectForKey:@"new_link"];
    
    NSURL *URL = [NSURL URLWithString:string];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[string, URL] applicationActivities:nil];
    
    activityViewController.excludedActivityTypes = @[UIActivityTypeAirDrop, UIActivityTypeAddToReadingList,UIActivityTypePrint,@"com.apple.reminders.RemindersEditorExtension"];
    
    [self.navigationController presentViewController:activityViewController animated:YES completion:^{
    }];
}

@end
