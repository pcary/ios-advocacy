//
//  AppDelegate.h
//  ahaactioncenter
//
//  Created by Server on 4/4/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"

@class MSDynamicsDrawerViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MSDynamicsDrawerViewController *dynamicsDrawerViewController;
@property (strong, nonatomic) UISplitViewController *splitViewController;
@property (strong, nonatomic) MenuViewController *menuViewController;

- (void)openSideMenu;
- (void)toggleMenu;
- (void)getFeed;

@end

