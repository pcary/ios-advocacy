//
//  ActionCenterManager+CreateUser.m
//  ahaactioncenter
//
//  Created by Cary, Patrick on 12/28/15.
//  Copyright © 2015 AHA. All rights reserved.

#import "ActionCenterManager+CreateUser.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "UIViewController+TopMostController.h"
#import "ProgressHUD.h"
#import "MBProgressHUD.h"
#import "JNKeychain.h"

@implementation ActionCenterManager (CreateUser)

- (void)verifyCorrectlyEmail:(NSString *)email completion:(void(^)(BOOL isValid))completion
{
    ProgressHUD *hud = [ProgressHUD sharedInstance];
    
    NSString *password = [JNKeychain loadValueForKey:@"somethingOnlyWeKnow"];
    NSString *strUrl = [NSString stringWithFormat:@"http://ahaconnect.org/apis/sso.php?email=%@&password=%@&db=prod", email, password];
    NSURL *url = [NSURL URLWithString:[self encodeURL:strUrl]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:20.0];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *output = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"%@", output);
        
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];

        if ([responseDict[@"status"] isEqualToString:@"found user"]) {
            OAM *oam = [[OAM alloc] initWithJSONData:responseObject];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setBool:YES forKey:@"isLoggedIn"];
            [prefs setBool:YES forKey:@"showTip"];
            [prefs setBool:NO forKey:@"inVoterVoice"];
            [prefs setObject:oam.email forKey:@"email"];
            [prefs setObject:(oam.first_name == nil) ? @"" : oam.first_name forKey:@"firstName"];
            [prefs setObject:(oam.last_name == nil) ? @"" : oam.last_name forKey:@"lastName"];
            [prefs setObject:(oam.address_line == nil) ? @"" : oam.address_line forKey:@"address"];
            [prefs setObject:(oam.city == nil) ? @"" : oam.city forKey:@"city"];
            [prefs setObject:(oam.state == nil) ? @"" : oam.state forKey:@"state"];
            [prefs setObject:(oam.zip == nil) ? @"" : oam.zip forKey:@"zip"];
            [prefs setObject:(oam.prefix == nil) ? @"" : oam.prefix forKey:@"prefix"];
            [prefs setObject:(oam.phone == nil) ? @"" : oam.phone forKey:@"phone"];
            [prefs setObject:(oam.ahaid == nil) ? @"" : oam.ahaid forKey:@"contactId"];
            [prefs synchronize];
            
            completion(YES);
        } else {
            completion(NO);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completion(NO);
    }];
    
    [operation start];
}

/*
- (void)verifyCorrectlyEmailID:(OAM *)oam withID:(NSString *)verificationID completion:(void(^)(BOOL isValid))completion
{
    NSString *format = @"http://54.245.255.190/p/action_center/api/v1/emailVerification?email=%@&firstName=%@&address=%@&zipcode=%@&country=US&lastName=%@";
    
    NSString *strUrl = [NSString stringWithFormat:format,
                        oam.email,
                        oam.first_name,
                        oam.address_line,
                        oam.zip,
                        @"US",
                        oam.last_name];
    NSURL *url = [NSURL URLWithString:[self encodeURL:strUrl]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:20.0];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
        NSString *output = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"%@", output);
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (completion) {
            BOOL isEmailValid = [responseDict[@"response"][@"status"] integerValue] == 200;
            completion(isEmailValid);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (completion) {
            completion(NO);
        }
    }];
    
    [operation start];
}
*/
//save the user

/*
 
 how can we get the latest oam object matching some user's contactId?
 */

- (void)createUser:(OAM *)oam withContactId:(NSString *)contactId completion:(CompletionVoterVoiceNew)completion
{
    [self createVerifiedUser:oam withContactId:contactId completion:completion];
}

- (void)createVerifiedUser:(OAM *)oam withContactId:(NSString *)contactId completion:(CompletionVoterVoiceNew)completion
{
    NSDictionary *paramters = @{
                                @"id" : contactId, // your internal identifier for the person
                                //@"groupList" : @"", // (Optional) the numeric identifier of a contact list to search (when searching a non-default list)
                                };
    // Find existing user in the VoterVoice DB
    [self.requestManager GET:@"getIdentities" parameters:paramters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        /*
         {
             id,
             givenNames,
             surname,
             token
         }
         */
        
        NSArray *matches = responseObject[@"response"][@"body"];
        
        if ([matches count] == 1) {
            // A matching user was found
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            NSDictionary *userDictionary = [matches lastObject];
            NSMutableDictionary *userParamters = [userDictionary mutableCopy];
            [userParamters addEntriesFromDictionary:@{
                                                      @"givenNames" : [prefs objectForKey:@"firstName"],
                                                      @"surname" : [prefs objectForKey:@"lastName"],
                                                      @"email" : [prefs objectForKey:@"email"],
                                                      @"org_name" : @"",
                                                      @"phone" : [prefs objectForKey:@"phone"],
                                                      @"prefix" : [prefs objectForKey:@"prefix"] ? [prefs objectForKey:@"prefix"] : @"M.",
                                                      @"status" : [prefs objectForKey:@"status"],
                                                      @"address" : [prefs objectForKey:@"address"],
                                                      @"city" : [prefs objectForKey:@"city"],
                                                      @"county" : @"",
                                                      @"zipcode" : [prefs objectForKey:@"zip"],
                                                      @"state" : [prefs objectForKey:@"state"],
                                                      @"country" : @"US",
                                                      }];
            
            // Build a JSON in a VoterVoice user structure from OAM and send it as a JSONSerialized paramters to the users endpoint
            
            [self.requestManager POST:@"putUserProfile" parameters:userParamters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                // Success
                if (completion) {
                    [[NSUserDefaults standardUserDefaults] setObject:userDictionary[@"token"] forKey:@"token"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    completion(userDictionary[@"id"], userDictionary[@"token"], nil);
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                // Failure
                if (completion) {
                    completion(nil, nil, error);
                }
            }];
            
        } else if ([matches count] == 0) {
            
            // No matches
            [self createUser:oam withEmail:oam.cst_eml_address_dn completion:^(NSString *userId, NSString *token, NSError *error) {
                
                if (error) {
                    if (completion) {
                        completion(nil, nil, error);
                    }
                    return;
                }
                
                NSDictionary *updateContactIdParaemters = @{
                                                            @"userid" : userId,
                                                            @"id" : contactId,
                                                            @"token" : token
                                                                };
                
                [self.requestManager GET:@"updateContactId" parameters:updateContactIdParaemters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    // Success
                    if (completion) {
                        completion(userId, token, nil);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    // Failure
                    if (completion) {
                        completion(nil, nil, error);
                    }
                }];
            }];
        
        } else {
            // error
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    // either
    // Create new User
    // Update existing one
}



@end
