//
//  VoterVoiceMatches.m
//  ahaactioncenter
//
//  Created by Vince Davis on 4/7/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import "VoterVoiceMatches.h"

@implementation VoterVoiceMatches

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.country forKey:@"country"];
    [coder encodeBool:self.canSend forKey:@"canSend"];
    [coder encodeObject:self.id forKey:@"id"];
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeObject:self.type forKey:@"type"];
}

-(id)initWithCoder:(NSCoder *)decoder {
    if(self = [super init]){
        self.country = [decoder decodeObjectForKey:@"country"];
        self.canSend = [decoder decodeBoolForKey:@"canSend"];
        self.id = [decoder decodeObjectForKey:@"id"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.type = [decoder decodeObjectForKey:@"type"];
    }
    return self;
}

@end
