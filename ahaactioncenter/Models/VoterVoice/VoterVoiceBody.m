//
//  VoterVoiceBody.m
//  ahaactioncenter
//
//  Created by Vince Davis on 4/6/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import "VoterVoiceBody.h"

@implementation VoterVoiceBody

-(id)init {
    self = [super init];
    if (self) {
        [self setValue:@"VoterVoiceAddress" forKeyPath:@"propertyArrayMap.addresses"];
        [self setValue:@"VoterVoiceMatches" forKeyPath:@"propertyArrayMap.matches"];
        [self setValue:@"VoterVoiceMessage" forKeyPath:@"propertyArrayMap.messages"];
        [self setValue:@"VoterVoiceSelectedAnswers" forKeyPath:@"propertyArrayMap.preSelectedAnswers"];
        [self setValue:@"VoterVoiceTargets" forKeyPath:@"propertyArrayMap.targets"];
        [self setValue:@"VoterVoiceSection" forKeyPath:@"propertyArrayMap.sections"];
    }
    return self;
}

- (NSString *)sponsorType
{
    NSString *string = self.groupId;
    
    if ([string rangeOfString:@"not"].location == NSNotFound) {
        if ([string rangeOfString:@"co-sponsors"].location == NSNotFound) {
            return [NSString stringWithFormat:@"Sponsor"];
        } else {
        return [NSString stringWithFormat:@"Co-Sponsor"];
        }
    } else {
        return [NSString stringWithFormat:@"Non-Sponsor"];
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.givenNames forKey:@"givenNames"];
    [aCoder encodeObject:self.id forKey:@"id"];
    [aCoder encodeObject:self.surname forKey:@"surname"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.message forKey:@"message"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.userToken forKey:@"userToken"];
    [aCoder encodeObject:self.suggestedZipCode forKey:@"suggestedZipCode"];
    [aCoder encodeObject:self.addresses forKey:@"addresses"];
    [aCoder encodeObject:self.groupId forKey:@"groupId"];
    [aCoder encodeObject:self.messageId forKey:@"messageId"];
    [aCoder encodeObject:self.matches forKey:@"matches"];
    [aCoder encodeObject:self.contactId forKey:@"contactId"];
    
    [aCoder encodeObject:self.alert forKey:@"alert"];
    [aCoder encodeObject:self.headline forKey:@"headline"];
    [aCoder encodeBool:self.isPrivate forKey:@"isPrivate"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.priority forKey:@"priority"];
    [aCoder encodeBool:self.targetsFederal forKey:@"targetsFederal"];
    [aCoder encodeBool:self.targetsState forKey:@"targetsState"];
    [aCoder encodeObject:self.timeFrame forKey:@"timeFrame"];
    [aCoder encodeObject:self.start forKey:@"start"];
    [aCoder encodeObject:self.end forKey:@"end"];
    [aCoder encodeObject:self.messageDisplay forKey:@"messageDisplay"];
    [aCoder encodeObject:self.displayName forKey:@"displayName"];
    [aCoder encodeObject:self.imageUrl forKey:@"imageUrl"];
    [aCoder encodeObject:self.phoneCallMethod forKey:@"phoneCallMethod"];
    
    [aCoder encodeObject:self.preSelectedAnswers forKey:@"preSelectedAnswers"];
    [aCoder encodeObject:self.targets forKey:@"targets"];
    [aCoder encodeObject:self.messages forKey:@"messages"];
    [aCoder encodeObject:self.sections forKey:@"sections"];
}



-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        self.givenNames = [aDecoder decodeObjectForKey:@"givenNames"];
        self.id = [aDecoder decodeObjectForKey:@"id"];
        self.surname = [aDecoder decodeObjectForKey:@"surname"];
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.message = [aDecoder decodeObjectForKey:@"message"];
        self.userId = [aDecoder decodeObjectForKey:@"userId"];
        self.userToken = [aDecoder decodeObjectForKey:@"userToken"];
        self.suggestedZipCode = [aDecoder decodeObjectForKey:@"suggestedZipCode"];
        self.addresses = [aDecoder decodeObjectForKey:@"addresses"];
        self.groupId = [aDecoder decodeObjectForKey:@"groupId"];
        self.messageId = [aDecoder decodeObjectForKey:@"messageId"];
        self.matches = [aDecoder decodeObjectForKey:@"matches"];
        self.contactId = [aDecoder decodeObjectForKey:@"contactId"];
        
        self.alert = [aDecoder decodeObjectForKey:@"alert"];
        self.headline = [aDecoder decodeObjectForKey:@"headline"];
        self.isPrivate = [aDecoder decodeObjectForKey:@"isPrivate"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.priority = [aDecoder decodeObjectForKey:@"priority"];
        self.targetsFederal = [aDecoder decodeObjectForKey:@"targetsFederal"];
        self.targetsState = [aDecoder decodeObjectForKey:@"targetsState"];
        self.timeFrame = [aDecoder decodeObjectForKey:@"timeFrame"];
        self.start = [aDecoder decodeObjectForKey:@"start"];
        self.end = [aDecoder decodeObjectForKey:@"end"];
        self.messageDisplay = [aDecoder decodeObjectForKey:@"messageDisplay"];
        self.displayName = [aDecoder decodeObjectForKey:@"displayName"];
        self.imageUrl = [aDecoder decodeObjectForKey:@"imageUrl"];
        self.phoneCallMethod = [aDecoder decodeObjectForKey:@"phoneCallMethod"];
        
        self.preSelectedAnswers = [aDecoder decodeObjectForKey:@"preSelectedAnswers"];
        self.targets = [aDecoder decodeObjectForKey:@"targets"];
        self.messages = [aDecoder decodeObjectForKey:@"messages"];
        self.sections = [aDecoder decodeObjectForKey:@"sections"];
    }
    return self;
}

@end
