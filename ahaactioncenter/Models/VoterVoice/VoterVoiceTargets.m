//
//  VoterVoiceTargets.m
//  ahaactioncenter
//
//  Created by Vince Davis on 4/7/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import "VoterVoiceTargets.h"

@implementation VoterVoiceTargets

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.displayCategory forKey:@"displayCategory"];
    [aCoder encodeObject:self.displayName forKey:@"displayName"];
    [aCoder encodeObject:self.groupId forKey:@"groupId"];
 }
 
 -(id)initWithCoder:(NSCoder *)aDecoder{
     if(self = [super init]){
         self.displayCategory = [aDecoder decodeObjectForKey:@"displayCategory"];
         self.displayName = [aDecoder decodeObjectForKey:@"displayName"];
         self.groupId = [aDecoder decodeObjectForKey:@"groupId"];
     }
 return self;
 }

@end
