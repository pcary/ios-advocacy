//
//  TESTAHANewsDetailsViewController.m
//  ahaactioncenter
//
//  Created by Cary, Patrick on 8/20/18.
//  Copyright © 2018 AHA. All rights reserved.
//

#import "TESTAHANewsDetailsViewController.h"

@interface TESTAHANewsDetailsViewController ()

@end

@implementation TESTAHANewsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"%ld", (long)self.tag);
    
    if (self.tag == 2) {
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        paragraphStyle.headIndent = 17;
        paragraphStyle.firstLineHeadIndent = 17;
        paragraphStyle.lineSpacing = 4;
        
        NSString *string = @"DEA proposes reducing opioid production quotas for 2019";
        
        self.newsTitle.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSParagraphStyleAttributeName: paragraphStyle}];
        
        self.newsTitle.numberOfLines = 2;
        self.newsTitle.minimumScaleFactor = 0.5;
        self.newsTitle.adjustsFontSizeToFitWidth = YES;
        
        self.newsTitle.alpha = .95f;
        self.newsTitle.layer.shadowOpacity = 1.0;
        self.newsTitle.layer.shadowRadius = 0.0;
        self.newsTitle.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.newsTitle.layer.shadowOffset = CGSizeMake(0,0.5);
        
        self.title = @"DEA Proposes Reducing Opioid...";
        self.newsImage.image = [UIImage imageNamed:@"test_opiod"];
        
        //self.newsTitle.attributedText = string;
        self.newsDescription.text = @"The Drug Enforcement Administration today issued its proposed 2019 aggregate production quotas for certain controlled substances, which the agency said would reduce manufacturing quotas for six frequently misused opioids by an average 10 percent. The proposed rule will be published in the Aug. 20 Federal Register with comments accepted for 30 days. AHA and others have urged DEA to include drug shortages as a factor when setting and adjusting aggregate and individual manufacturers’ production quotas for controlled substances.";
        
    } else if (self.tag == 3) {
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        paragraphStyle.headIndent = 17;
        paragraphStyle.firstLineHeadIndent = 17;
        paragraphStyle.lineSpacing = 4;
        
        NSString *string = @"CMS grants continued approval to hospital accrediting organization";
        
        self.newsTitle.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSParagraphStyleAttributeName: paragraphStyle}];
        
        self.newsTitle.numberOfLines = 2;
        self.newsTitle.minimumScaleFactor = 0.5;
        self.newsTitle.adjustsFontSizeToFitWidth = YES;
        
        self.newsTitle.alpha = .95f;
        self.newsTitle.layer.shadowOpacity = 1.0;
        self.newsTitle.layer.shadowRadius = 0.0;
        self.newsTitle.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.newsTitle.layer.shadowOffset = CGSizeMake(0,0.5);
        
        self.title = @"CMS Grants Continued Approval...";
        
        self.newsImage.image = [UIImage imageNamed:@"test_cms"];
        
        self.newsDescription.text = @"The Centers for Medicare & Medicaid Services has approved DNV GL – Healthcare for continued recognition as a national accrediting organization for hospitals that wish to participate in the Medicare and Medicaid programs. The decision is effective for four years.";
        
    } else if (self.tag == 0) {
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        paragraphStyle.headIndent = 17;
        paragraphStyle.firstLineHeadIndent = 17;
        paragraphStyle.lineSpacing = 4;
        
        NSString *string = @"Artificial Intelligence can support health care leaders, but it’s not a complete solution, expert says";
        
        self.newsTitle.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSParagraphStyleAttributeName: paragraphStyle}];
        
        self.newsTitle.numberOfLines = 2;
        self.newsTitle.minimumScaleFactor = 0.5;
        self.newsTitle.adjustsFontSizeToFitWidth = YES;
        
        self.newsTitle.alpha = .95f;
        self.newsTitle.layer.shadowOpacity = 1.0;
        self.newsTitle.layer.shadowRadius = 0.0;
        self.newsTitle.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.newsTitle.layer.shadowOffset = CGSizeMake(0,0.5);
        
        self.title = @"Artificial Intelligence can support...";
        self.newsImage.image = [UIImage imageNamed:@"test_ai"];
        
        //self.newsTitle.attributedText = string;
        self.newsDescription.text = @"Artificial intelligence, or AI, can support health care leaders to a staggering degree, as a recent webinar hosted by the AHA’s Health Forum and sponsored by GE Healthcare illustrates. It can anticipate patient outcomes, help assess and treat nuanced conditions, and give clinicians valuable evidence to aid them in complicated decision-making. On top of that, its ability to supplement clinical documentation errors and to mine the sometimes-unstructured electronic health record can save time and money.";
        
        
    } else if (self.tag == 1) {
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        paragraphStyle.headIndent = 17;
        paragraphStyle.firstLineHeadIndent = 17;
        paragraphStyle.lineSpacing = 4;
        
        NSString *string = @"How hospitals are using telehealth to improve care and access";
        
        self.newsTitle.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSParagraphStyleAttributeName: paragraphStyle}];
        
        self.newsTitle.numberOfLines = 2;
        self.newsTitle.minimumScaleFactor = 0.5;
        self.newsTitle.adjustsFontSizeToFitWidth = YES;
        
        self.newsTitle.alpha = .95f;
        self.newsTitle.layer.shadowOpacity = 1.0;
        self.newsTitle.layer.shadowRadius = 0.0;
        self.newsTitle.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.newsTitle.layer.shadowOffset = CGSizeMake(0,0.5);
        
        self.title = @"How hospitals are using telehealth...";
        self.newsImage.image = [UIImage imageNamed:@"test_tele"];
        
        //self.newsTitle.attributedText = string;
        self.newsDescription.text = @"Mount Sinai Queens in New York City is learning how to better care for stroke victims via a remote expert; Bryan Health in Lincoln, Neb. is putting part of its emergency department online; and Children’s Hospital & Medical Center in Omaha, Neb. is bringing behavioral health care to children in remote places. Read on for details. Through Telehealth, Rural Pediatric Patients Have a Better Chance at Behavioral Health Care Children’s Hospital & Medical Center in Omaha is the only full-service pediatric health care center in Nebraska, a state which also is facing a psychiatrist shortage. Because of this, pediatric patients from rural, western Nebraska often traveled up to 4.5 hours to reach the hospital for psychiatric appointments. Children’s Hospital was able to reduce that time dramatically — to the tune of 96,000 miles a year per patient — by instituting a telepsychiatry program with guidance from a telehealth savvy psychiatrist.Today, Children’s works with eight sites, using video conferencing equipment to connect with remote psychiatrists. Now, the hospital is gaining patients (it currently serves 250,000 children), and patients are more likely to show up to scheduled appointments. The enhanced access helped the hospital to attract providers, as well. Read the full case study here.";
            
        } else {
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        
        paragraphStyle.headIndent = 17;
        paragraphStyle.firstLineHeadIndent = 17;
        paragraphStyle.lineSpacing = 4;
        
        NSString *string = @"DEA proposes reducing opioid production quotas for 2019";
        
        self.newsTitle.attributedText = [[NSAttributedString alloc] initWithString:string attributes:@{NSParagraphStyleAttributeName: paragraphStyle}];
        
        self.newsTitle.numberOfLines = 2;
        self.newsTitle.minimumScaleFactor = 0.5;
        self.newsTitle.adjustsFontSizeToFitWidth = YES;
        
        self.newsTitle.alpha = .95f;
        self.newsTitle.layer.shadowOpacity = 1.0;
        self.newsTitle.layer.shadowRadius = 0.0;
        self.newsTitle.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.newsTitle.layer.shadowOffset = CGSizeMake(0,0.5);
        
        self.title = @"DEA Proposes Reducing Opioid...";
        self.newsImage.image = [UIImage imageNamed:@"test_opiod"];
        
        //self.newsTitle.attributedText = string;
        self.newsDescription.text = @"The Drug Enforcement Administration today issued its proposed 2019 aggregate production quotas for certain controlled substances, which the agency said would reduce manufacturing quotas for six frequently misused opioids by an average 10 percent. The proposed rule will be published in the Aug. 20 Federal Register with comments accepted for 30 days. AHA and others have urged DEA to include drug shortages as a factor when setting and adjusting aggregate and individual manufacturers’ production quotas for controlled substances.";
    }
}



@end
