//
//  UIViewController+TopMostController.h
//  ahaactioncenter
//
//  Created by Cary, Patrick on 1/5/16.
//  Copyright © 2016 AHA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TopMostController)
+ (UIViewController*) topMostController;

@end
