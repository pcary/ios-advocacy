//
//  UIViewController+TopMostController.m
//  ahaactioncenter
//
//  Created by Cary, Patrick on 1/5/16.
//  Copyright © 2016 AHA. All rights reserved.
//

#import "UIViewController+TopMostController.h"
#import "AppDelegate.h"

@implementation UIViewController (TopMostController)

+ (UIViewController*) topMostController
{
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIViewController *topController = ad.window.rootViewController;
    
    while (topController.presentedViewController && ![topController.presentedViewController isKindOfClass:[UIAlertController class]]) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


@end
