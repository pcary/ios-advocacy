//
//  AHANewsViewController.h
//  ahaactioncenter
//
//  Created by Cary, Patrick on 3/6/19.
//  Copyright © 2019 AHA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Haneke/Haneke.h>

NS_ASSUME_NONNULL_BEGIN

@interface AHANewsViewController : UIViewController

@property (nonatomic, assign) NSDictionary *articleDict;
@property (nonatomic, strong) NSString *articleTitle;
@property (strong, nonatomic) IBOutlet UIImageView *mainImageView;
@property (strong, nonatomic) IBOutlet UILabel *headlineLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@end

NS_ASSUME_NONNULL_END
