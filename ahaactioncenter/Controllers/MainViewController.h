//
//  MainViewController.h
//  ahaactioncenter
//
//  Created by Server on 4/5/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NSDate_TimeAgo/NSDate+TimeAgo.h>

typedef enum {
    kContentTypeNews,
    kContentTypeCampaigns
} kContentType;

@interface MainViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *newsButton;
@property (strong, nonatomic) IBOutlet UIButton *campaignsButton;

@property (nonatomic, assign) kContentType kContentType;

@end
