//
//  GeneralTableViewController.h
//  ahaactioncenter
//
//  Created by Vince Davis on 4/7/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kViewTypeTwitter,
    kViewTypeCampaign,
    kViewTypeDirectory,
    kViewTypeAHANews,
    kViewTypeContactUs,
    kViewTypeDefault,
    kViewTypeRecipients,
    kViewTypeOfficials,
    kViewTypeByCommittee,
    kViewTypeByLastName,
    kViewTypeByParty,
    kViewTypeFilterMenu
} kViewType;

@interface GeneralTableViewController : UIViewController

@property(nonatomic, assign) kViewType viewType;
@property(nonatomic, assign) BOOL viewShouldRefresh;
@property(nonatomic, retain) NSDictionary *dict;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewSort;
@property (weak, nonatomic) IBOutlet UIImageView *menuArrow;

@property(nonatomic, retain) NSString *campaignID;
@property (strong, nonatomic) NSArray *data;

@property(nonatomic, retain) NSDictionary *officialsSectionArrays;
@property(nonatomic, retain) NSDictionary *committeeSectionArrays;
@property(nonatomic, retain) NSDictionary *lastNameSectionArrays;
@property(nonatomic, retain) NSDictionary *partySectionArrays;

@property(nonatomic, retain) NSArray *officialsSectionNames;
@property(nonatomic, retain) NSArray *committeeSectionNames;
@property(nonatomic, retain) NSArray *lastNameSectionNames;
@property(nonatomic, retain) NSArray *partySectionNames;

@property(nonatomic, retain) NSArray *searchOfficialSectionNames;

@property (nonatomic, strong) NSCache *imageCache;

@property (nonatomic, strong) NSArray *selectedList;
@property (nonatomic, strong) NSString *nameString;

//@property UISearchBar *searchBar;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarBottomConstraint;

@end
