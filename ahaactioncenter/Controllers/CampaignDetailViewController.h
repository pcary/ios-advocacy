//
//  CampaignDetailViewController.h
//  ahaactioncenter
//
//  Created by Vince Davis on 4/6/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoterVoiceBody.h"
@import CFAlertViewController;

@interface CampaignDetailViewController : UIViewController

@property(nonatomic, retain) NSString *campaignID;
@property(nonatomic, retain) NSString *groupID;
@property(nonatomic, retain) NSArray *bodies;
@property(nonatomic, retain) NSDictionary *profile;
@property(nonatomic, strong) VoterVoiceMatches *currentMatch;
@property (nonatomic) NSInteger currentIndex;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *messageLabel;

@end
