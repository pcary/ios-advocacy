//
//  LoginViewController.m
//  ahaactioncenter
//
//  Created by Server on 4/5/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import "LoginViewController.h"
#import "ActionCenterManager.h"
#import "ProgressHUD.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "MenuViewController.h"
#import "MainViewController.h"
#import "Constants.h"
#import "JNKeychain.h"

@interface LoginViewController ()<UITextFieldDelegate>
{
    ActionCenterManager *action;
    ProgressHUD *hud;
}

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (weak, nonatomic) NSString *loginKey;

@property BOOL revealPassword;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    action = [ActionCenterManager sharedInstance];
    hud = [ProgressHUD sharedInstance];
    
    self.revealPassword = false;
    self.passwordField.secureTextEntry = true;
    [self.passwordEyeButton setImage:[UIImage imageNamed:@"eye_off"] forState:UIControlStateNormal];
    
    self.resetPasswordButton.hidden = true;
    self.createAccountButton.hidden = true;
    
    self.loginLabel.layer.masksToBounds = true;
    self.loginLabel.layer.cornerRadius = 3.0;
    
    self.emailLabel.layer.masksToBounds = true;
    self.emailLabel.layer.borderColor = kAHABlue.CGColor;
    self.emailLabel.layer.borderWidth = 0.5;
    self.emailLabel.layer.cornerRadius = 3.0;
    
    self.passwordLabel.layer.masksToBounds = true;
    self.passwordLabel.layer.borderColor = kAHABlue.CGColor;
    self.passwordLabel.layer.borderWidth = 0.5;
    self.passwordLabel.layer.cornerRadius = 3.0;
    
    self.emailField.delegate = self;
    self.passwordField.delegate = self;
    
    [self.emailField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0];
    [self.emailField setReturnKeyType:UIReturnKeyDefault];
    [self.passwordField setReturnKeyType:UIReturnKeyDefault];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
    
}
- (void) launchURLInBrowser:(NSURL *)url {
    if (![[UIApplication sharedApplication] openURL:url]) {
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
    }
}

- (void)bypassVoterVoice {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:@"isLoggedIn"];
    //[hud showHUDSucces:YES withMessage:@"Success"];
    [prefs setBool:NO forKey:@"inVoterVoice"];
    [prefs setBool:YES forKey:@"showTip"];
    [prefs setObject:_emailField.text forKey:@"email"];
    [prefs synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"bypass");
}

+ (void)resetViews {
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIStoryboard *sb = [[ad.window rootViewController] storyboard];
    MainViewController *main = (MainViewController *)[sb instantiateViewControllerWithIdentifier:@"main"];
    UINavigationController *mainNav = [[UINavigationController alloc] initWithRootViewController:main];
    mainNav.toolbarHidden = NO;
    MenuViewController *menu = (MenuViewController *)[sb instantiateViewControllerWithIdentifier:@"menu"];
    UINavigationController *menuNav = [[UINavigationController alloc] initWithRootViewController:menu];
    menuNav.toolbarHidden = NO;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        UISplitViewController *split = (UISplitViewController *)ad.splitViewController;
        [split setViewControllers:@[menuNav, mainNav]];
        NSLog(@"ipad");
    }
    else {
        MSDynamicsDrawerViewController *dynamic = (MSDynamicsDrawerViewController *)ad.dynamicsDrawerViewController;
        [dynamic setPaneState:MSDynamicsDrawerPaneStateClosed];
        [dynamic setPaneViewController:mainNav animated:NO completion:nil];
        NSLog(@"iphone");
    }
}

- (NSString *)HMAC_SHA512_WithSecretString:(NSString*)secret string:(NSString *)string {
    
    CCHmacContext ctx;
    const char       *key = [secret UTF8String];
    const char       *str = [string UTF8String];
    unsigned char    mac[CC_SHA512_DIGEST_LENGTH];
    char             hexmac[2 * CC_SHA512_DIGEST_LENGTH + 1];
    char             *p;
    
    CCHmacInit( &ctx, kCCHmacAlgSHA512, key, strlen( key ));
    CCHmacUpdate( &ctx, str, strlen(str) );
    CCHmacFinal( &ctx, mac );
    
    p = hexmac;
    
    for (int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++ ) {
        snprintf( p, 3, "%02x", mac[ i ] );
        p += 2;
    }
    return [NSString stringWithUTF8String:hexmac];
}

- (IBAction)login:(id)sender {
    
    self.loginKey = @"dcc214a940022e3a33835dfa0784ef5aff44ef4c3592ca660d64bae64e9c662c";
    
    NSString *hashedPass = [self HMAC_SHA512_WithSecretString:self.loginKey string:_passwordField.text];
    NSLog(@"hashed password is: %@", hashedPass);
    
     if ([action isReachable]) {
         
         [hud showHUDWithMessage:@"Signing In"];
         [action getOAMUser:_emailField.text withPassword:hashedPass completion:^(OAM *oam, NSError *err) {
             
             if (!err) {
                
                 if (oam.status == 200) {

                     // Save the password to keychain.
                    // [JNKeychain saveValue:_passwordField.text forKey:@"somethingOnlyWeKnow"];
                     
                     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                     
                     // Create user in VoterVoice.
                     [action createUser:oam withContactId:[prefs objectForKey:@"contactId"] completion:^(NSString *userId, NSString *token, NSError *err) {
                         
                         [prefs setBool:YES forKey:@"isLoggedIn"];
                         [prefs setBool:YES forKey:@"showTip"];
                         [prefs setBool:NO forKey:@"inVoterVoice"];
                         
                         [self dismissViewControllerAnimated:YES completion:nil];
                         [hud showHUDSucces:YES withMessage:@"Complete"];
                         
                         AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                         
                         MainViewController *main = (MainViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"main"];
                         UINavigationController *mainNav = [[UINavigationController alloc] initWithRootViewController:main];
                         
                         
                //iPad
                if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
                    
                    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    
                    MenuViewController *menu = (MenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"menu"];
                    UINavigationController *menuNav = [[UINavigationController alloc] initWithRootViewController:menu];

                    UISplitViewController *split = (UISplitViewController *)ad.splitViewController;
                    [split setViewControllers:@[menuNav, mainNav]];
                }
                                        
                //iPhone
                else {
                
                    MSDynamicsDrawerViewController *dynamic = (MSDynamicsDrawerViewController *)ad.dynamicsDrawerViewController;
                    [dynamic setPaneState:MSDynamicsDrawerPaneStateClosed];
                    [dynamic setPaneViewController:mainNav animated:NO completion:nil];
                    [mainNav topViewController];
                }
                                    }];
                        } else {
                            //User not found.
                            [hud showHUDSucces:NO withMessage:@"Failed"];
                            [self showAlert];
                        }
                    } else {
                        //Login error.
                        NSLog(@"%@", err);
                        [hud showHUDSucces:NO withMessage:@"Failed"];
                        [self showAlert];
                    }
                }];
    }
    else {
        //No internet.
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Internet" message:@"Please check your internet connection and try again." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^void (UIAlertAction *action) {
            }]];
        
        [self.navigationController presentViewController:alert animated:YES completion:nil];
    }
    
    AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    MainViewController *main = (MainViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"main"];
    UINavigationController *mainNav = [[UINavigationController alloc] initWithRootViewController:main];
    
    MSDynamicsDrawerViewController *dynamic = (MSDynamicsDrawerViewController *)ad.dynamicsDrawerViewController;
    [dynamic setPaneState:MSDynamicsDrawerPaneStateClosed];
    [dynamic setPaneViewController:mainNav animated:NO completion:nil];
    [mainNav topViewController];
}

- (void)showAlert
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Invalid Login Credentials"
                                                 message:@"The Email Address or Password you entered is incorrect. Please verify your credentials and try again."
                                                delegate:self
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil, nil];
    [av show];
}

- (IBAction)forgotPassword:(id)sender {

    NSURL *url = [NSURL URLWithString:@"http://www.aha.org/oam/forgot-password.dhtml?ahasite=AHA&goto=http://www.aha.org/oam-aha/oam/welcome.html"];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL:url];
        
        [self showViewController:safariVC sender:nil];
        
    } else {
        
        [[UIApplication sharedApplication]openURL:url];
    }
}

- (IBAction)passwordEyePressed:(id)sender {
    if (self.revealPassword == true) {
        self.revealPassword = false;
        self.passwordField.secureTextEntry = true;
        [self.passwordEyeButton setImage:[UIImage imageNamed:@"eye_off"] forState:UIControlStateNormal];
    } else {
        self.revealPassword = true;
        self.passwordField.secureTextEntry = false;
        [self.passwordEyeButton setImage:[UIImage imageNamed:@"eye_on"] forState:UIControlStateNormal];
    }
}

- (IBAction)needHelp:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"https://ahaconnect.org/contact.php"];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL:url];
        
        [self showViewController:safariVC sender:nil];
        
    } else {
        
        [[UIApplication sharedApplication]openURL:url];
    }
}

- (IBAction)createAccount:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"https://ams.aha.org/EWEB/DynamicPage.aspx?Site=AHA&webcode=Register&action=add&nostore=1&AHABU=AHA&RedirectURL=http%3A%2F%2Fwww.aha.org%3A80%2Fhospital-members%2Fadvocacy-issues%2Faction%2Findex.shtml"];
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL:url];
        
        [self showViewController:safariVC sender:nil];
        
    } else {
        
        [[UIApplication sharedApplication]openURL:url];
    }
}

@end
