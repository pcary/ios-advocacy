//
//  GeneralTableViewController.m
//  ahaactioncenter
//
//  Created by Vince Davis on 4/7/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import "GeneralTableViewController.h"
#import "ProfileViewController.h"
#import "TwitterTableViewCell.h"
#import "SpecialBulletinTableViewCell.h"
#import "ActionAlertTableViewCell.h"
#import "EventCalendarTableViewCell.h"
#import "ActivityFeedTableViewCell.h"
#import "AHAAdvisoryTableViewCell.h"
#import "MedicareTableViewCell.h"
#import "STTwitter.h"
#import "DataModels.h"
#import "ProgressHUD.h"
#import "ActionCenterManager.h"
#import "Constants.h"
#import "CampaignDetailView.h"
#import "CampaignDetailViewController.h"
#import "KGModal.h"
#import "WebViewController.h"
#import "UpdateUserViewController.h"
#import <AMPPreviewController/AMPPreviewController.h>
#import "AppDelegate.h"
#import "FontAwesomeKit.h"
#import "DirectoryTableViewCell.h"
#import "MBProgressHUD.h"
#import "PopOverTableViewController.h"
#import <Haneke/Haneke.h>

@interface GeneralTableViewController () <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
{

    NSArray *list;
    NSArray *responseBody;
    NSArray *nameArray;
    NSDictionary *dictionary;
    ProgressHUD *hud;
    MBProgressHUD *MBProgressHUD;
    ActionCenterManager *action;
    VoterVoice *voter;
    NSMutableArray *filteredDirectory;
    BOOL isSelected;
    BOOL isSearching;
    UIImageView *profileImage;
    
}

@end

@implementation GeneralTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isSelected = false;
    self.tableViewSort.hidden = YES;
    self.menuArrow.hidden = YES;
    self.tableViewSort.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    hud = [ProgressHUD sharedInstance];
    action = [ActionCenterManager sharedInstance];
    voter = [[VoterVoice alloc] init];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.navigationItem.leftBarButtonItem = [ActionCenterManager dragButton]; }
    else {
        self.navigationItem.leftBarButtonItem = [ActionCenterManager splitButton]; }
    
    [self backButton];
    [self loadCustomView];
}

-(void)backButton {
    
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];

}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (_viewType == kViewTypeDirectory) {
        self.tableViewTopConstraint.constant = 108;
        self.searchBar.hidden = NO;
    }
    if (_viewType ==kViewTypeOfficials) {
        self.tableViewTopConstraint.constant = 108;
        self.searchBar.hidden = NO;
    }
    if (_viewType == kViewTypeRecipients) {
        self.tableViewTopConstraint.constant = 108 - 44;
        self.searchBar.hidden = YES;
    }
    if (_viewType == kViewTypeCampaign) {
        self.tableViewTopConstraint.constant = 108 - 44;
        self.searchBar.hidden = YES;
    }
    if (_viewType == kViewTypeTwitter) {
        self.tableViewTopConstraint.constant = 108 - 44;
        self.searchBar.hidden = YES;
    }
    if (_viewType == kViewTypeAHANews) {
        self.tableViewTopConstraint.constant = 108 - 44;
        self.searchBar.hidden = YES;
    }
}

- (void)loadCustomView
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if (_viewType == kViewTypeTwitter && _viewShouldRefresh) {
        self.title = @"Twitter";
        
        [hud showHUDWithMessage:@"Getting Tweets"];
        _viewShouldRefresh = NO;
        
        if (![action isReachable]) {
            return;
        }
        NSMutableArray *items = [NSMutableArray new];
        STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:kTwitterKey
                                                                consumerSecret:kTwitterSecret];
        
        [twitter verifyCredentialsWithSuccessBlock:^(NSString *bearerToken) {
            
            [twitter getUserTimelineWithScreenName:_dict[@"title"]
                                      successBlock:^(NSArray *statuses) {
                                          for (NSDictionary *d in statuses) {
                                              UserStatusTwitterItem *item = [UserStatusTwitterItem modelObjectWithDictionary:d];
                                              
                                              [items addObject:item];
                                          }
                                          list = items;
                                          [self.tableView reloadData];
                                          sleep(1);
                                          
                                          [hud showHUDSucces:YES withMessage:@"Loaded"];
                                          //[progress hideAnimated:YES];

                                      } errorBlock:^(NSError *error) {
                                          
                                      }];
        } errorBlock:^(NSError *error) {
        }];
    }
    if (_viewType == kViewTypeDirectory && _viewShouldRefresh) {
        
        [self viewWillLayoutSubviews];
        self.title = @"My Directory";
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        UIImage *filterImage = [UIImage imageNamed:@"filter"];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:filterImage
                                                                                  style:UIBarButtonItemStyleDone
                                                                                 target:self
                                                                                 action:@selector(officialsButtonPressed)];
        
        
        /* //KEEP THE NATIVE PROCESS GOING!//
        NSData *decodedObject = [prefs objectForKey:@"encodedArray"];
        NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:decodedObject];
        
        NSLog(@"%@", array);
        list = array;
        [self.tableView reloadData]; */
        
        [action getMatchesForCampaign:nil
                        withToken:[prefs objectForKey:@"token"]
                           completion:^(VoterVoice *votervoice, NSError *error){
            
                               if (!error) {
                                   NSMutableArray *arr = [[NSMutableArray alloc] init];
                
                                   for (VoterVoiceBody *body in votervoice.response.body) {
                                       if ([body.groupId isEqualToString:@"US Representative"] ||
                                           [body.groupId isEqualToString:@"US Senators"])
                                       {
                                           [arr addObject:body];
                                       }
                                   }
                                   list = (NSArray *)arr;
                                   NSLog(@"%@", list);
                                   voter = votervoice;
                
                                   [self.tableView reloadData];
                                   _viewShouldRefresh = NO;
                
                               }
                           }];
    }
    
    if (_viewType == kViewTypeOfficials) {
        
        [hud showHUDWithMessage:@"Loading"];

        [action getOfficials:^(NSDictionary *dict, NSError *error) {
            [self viewWillLayoutSubviews];
            
            if (!error) {
                dictionary = dict;
                list = (NSArray *)[dictionary valueForKeyPath:@"response.body"];
                
                [hud showHUDSucces:YES withMessage:@"Complete"];
                
                self.title = @"Directory by State";
                [self.tableView reloadData];
                _viewShouldRefresh = NO;
            }
        }];
    }
    
    if (_viewType == kViewTypeByCommittee) {
        
        [hud showHUDWithMessage:@"Loading"];
        
        [action getOfficials:^(NSDictionary *dict, NSError *error) {
            [self viewWillLayoutSubviews];
            
            if (!error) {
                dictionary = dict;
                responseBody = (NSArray *)[dictionary valueForKeyPath:@"response.body"];
                list = [responseBody sortedArrayUsingComparator: ^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                    
                    NSString * surname1 = obj1[@"occupancy"][@"politician"][@"personName"][@"surname"];
                    NSString * surname2 = obj2[@"occupancy"][@"politician"][@"personName"][@"surname"];
                    
                    return [surname1 compare:surname2];
                }];
                
                [hud showHUDSucces:YES withMessage:@"Complete"];
                //[hud hideAnimated:YES];
                
                self.title = @"Directory by Body";
                [self.tableView reloadData];
                _viewShouldRefresh = NO;
            }
        }];
    }
    
    if (_viewType == kViewTypeByLastName) {
        
        [hud showHUDWithMessage:@"Loading"];
        //MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        //hud.label.text = NSLocalizedString(@"Loading...", @"HUD Loading");
        
        [action getOfficials:^(NSDictionary *dict, NSError *error) {
            
            [self viewWillLayoutSubviews];
            
            if (!error) {
                dictionary = dict;
                responseBody = (NSArray *)[dictionary valueForKeyPath:@"response.body"];
                list = [responseBody sortedArrayUsingComparator: ^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                    
                    NSString * surname1 = obj1[@"occupancy"][@"politician"][@"personName"][@"surname"];
                    NSString * surname2 = obj2[@"occupancy"][@"politician"][@"personName"][@"surname"];
                    
                    return [surname1 compare:surname2];
                }];
                
                [hud showHUDSucces:YES withMessage:@"Complete"];
                //[hud hideAnimated:YES];
                
                self.title = @"Directory by Last Name";
                [self.tableView reloadData];
                _viewShouldRefresh = NO;
            }
        }];
    }
    
    /* if (_viewType == kViewTypeByParty) {
        
        [hud showHUDWithMessage:@"Loading"];
        //MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        //hud.label.text = NSLocalizedString(@"Loading...", @"HUD Loading");
        
        [action getOfficials:^(NSDictionary *dict, NSError *error) {
            
            [self viewWillLayoutSubviews];
            
            if (!error) {
                dictionary = dict;
                responseBody = (NSArray *)[dictionary valueForKeyPath:@"response.body"];
                list = [responseBody sortedArrayUsingComparator: ^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                    
                    NSString * surname1 = obj1[@"occupancy"][@"politician"][@"personName"][@"surname"];
                    NSString * surname2 = obj2[@"occupancy"][@"politician"][@"personName"][@"surname"];
                    
                    return [surname1 compare:surname2];
                }];
                
                [hud showHUDSucces:YES withMessage:@"Complete"];
                //[hud hideAnimated:YES];
                
                self.title = @"Directory by Party";
                
                [self.tableView reloadData];
                _viewShouldRefresh = NO;
            }
        }];
    } */
    
    if (_viewType == kViewTypeRecipients && _viewShouldRefresh) {
        self.title = @"Select a Recipient";
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [hud showHUDWithMessage:@"Loading"];
        
        [action getMatchesForCampaign:_campaignID
                            withToken:[prefs objectForKey:@"token"]
                           completion:^(VoterVoice *votervoice, NSError *error){
                               
                               if (!error) {
                                
                                   NSMutableDictionary *bodyGroups = [NSMutableDictionary dictionary];
                                   
                                   for (VoterVoiceBody *body in votervoice.response.body) {
                                       
                                       if (bodyGroups[[body sponsorType]] != nil) {
                                           
                                           // Add the matches to an existing body with the same sponsor type
                                           // This enables us to have bodies with unique group ids while retaining all the matches
                                           
                                           VoterVoiceBody *existingBody = bodyGroups[[body sponsorType]];
                                           NSMutableArray *combinedMatches = [NSMutableArray arrayWithArray:existingBody.matches];
                                           [combinedMatches addObjectsFromArray:body.matches];
                                           
                                           existingBody.matches = combinedMatches;
                                           
                                       } else {
                                           bodyGroups[[body sponsorType]] = body;
                                       }
                                   }
                                   
                                   list = [bodyGroups allValues];
                                   NSLog(@"%@", list);
                                   voter = votervoice;
                                   
                                   [hud showHUDSucces:YES withMessage:@"Complete"];
                                   //[hud hideAnimated:YES];
                                   
                                   [self.tableView reloadData];
                                   _viewShouldRefresh = NO;
                               }
                           }];
    }
    if (_viewType == kViewTypeCampaign && _viewShouldRefresh) {
        self.title = @"Campaigns";
        
        [hud showHUDWithMessage:@"Getting Campaigns"];
        
        [action getCampaignSummaries:^(VoterVoice *votervoice, NSError *error){
            if (!error) {
                list = votervoice.response.body;
                voter = votervoice;
                
                [hud showHUDSucces:YES withMessage:@"Loaded"];

                [self.tableView reloadData];
                _viewShouldRefresh = NO;
            }
        }];
    }
    if (_viewType == kViewTypeAHANews) {
        self.title = @"AHA News";
        
        [action getAHANews:^(NSArray *news, NSError *error) {
            if (!error) {
                list = news;
                [self.tableView reloadData];
                sleep(0.5);
            }
        }];
    }
    if (_viewType == kViewTypeContactUs && _viewShouldRefresh) {
        self.title = @"Contact AHA";
        NSDictionary *general = @{@"title" : @"General Inquiry", @"text" : @"Send an email inquiry to the American Hospital Association"};
        NSDictionary *actionCenter = @{@"title" : @"Action Center Feedback", @"text" : @"It's important for us to know the results of your Congressional contacts.  Who did you weigh in with?  What issues were raised?  How did they respond?"};
        list = @[general, actionCenter];
        [self.tableView reloadData];
    }
    if (_viewType == kViewTypeFilterMenu) {
        
        self.data = [[NSArray alloc] initWithObjects:
                     @"Filter by Body",
                     @"Filter by Last Name",
                    // @"Filter by Party",
                     @"Filter by State",
                     @"My Directory", nil];
        
        [self.tableViewSort reloadData];
    }
    
}

-(void)randomMethod {
    
    if ([self.title isEqualToString:@"My Directory"]) {
        _viewType = kViewTypeDirectory;
    }
    
    
}

- (NSString *) cleanString:(NSString *)str {
    str = [str stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    str = [str stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    str = [str stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    return str;
}

- (void)sectionButtonClicked:(id)sender {
    GeneralTableViewController *vc = (GeneralTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"general"];
    vc.viewType = kViewTypeCampaign;
    vc.viewShouldRefresh = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)campaignFeedbackPressed:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.votervoice.net/AHA/Surveys/2526/Respond"]];
}

- (void)showPDF:(NSString *)link {
    AMPPreviewController *pc = [[AMPPreviewController alloc]
                                initWithRemoteFile:[NSURL URLWithString:link]];
    
    [pc setStartDownloadBlock:^(){
        NSLog(@"Start download");
        
    }];
    [pc setFinishDownloadBlock:^(NSError *error){
        NSLog(@"Download finished %@", error);
        
    }];
    [self.navigationController pushViewController:pc animated:YES];
}

- (void)showTwitter:(NSString *)string {
    //https://twitter.com/search?q=%23partners4quality&src=typd
    NSString *link;
    NSDictionary *d;
    WebViewController *vc = (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
    vc.shouldRefresh = YES;
    vc.webType = kWebTypeWeb;
    if ([string hasPrefix:@"#"]) {
        
        d = @{@"Title" : string };
        string = [string stringByReplacingOccurrencesOfString:@"#" withString:@"%23"];
        link = [NSString stringWithFormat:@"https://twitter.com/search?q=%@&src=typd", string];
    }
    if ([string hasPrefix:@"@"]) {
        d = @{@"Title" : string };
        string = [string stringByReplacingOccurrencesOfString:@"@" withString:@""];
        link = [NSString stringWithFormat:@"https://twitter.com/%@", string];
    }
    if ([string hasPrefix:@"http://"] || [string hasPrefix:@"https://"] ) {
        d = @{@"Title" : @"Link"};
        link = string;
    }
    
    vc.link = link;
    vc.dict = d;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requiredInfo
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Additional Info Needed"
                                                                   message:@"To enable matching you to your legislators, additional info is needed. Would you like to enter the needed info?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"NO"
                                              style:UIAlertActionStyleCancel
                                            handler:^void (UIAlertAction *action)
                      {
                          
                      }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"YES"
                                              style:UIAlertActionStyleDefault
                                            handler:^void (UIAlertAction *action)
                      {
                          UpdateUserViewController *update = [[UpdateUserViewController alloc] initWithStyle:UITableViewStyleGrouped];
                          update.excludeList = @[@"excludedList", @"showPhone"/*, @"firstName", @"lastName", @"prefix", @"phone"*/];
                          UserForm *form = (UserForm *)update.formController.form;
                          form.firstName = @"test";
                          update.validateAddress = YES;
                          update.showCancel = YES;
                          UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:update];
                          nav.modalPresentationStyle = UIModalPresentationFormSheet;
                          [self.navigationController presentViewController:nav animated:YES completion:nil];
                          
                      }]];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)showAlert:(NSString *)alert withMessage:(NSString *)msg {
    NSString *str;
    if ([alert containsString:@"address"]) {
        str = @"Your address on file does not match U.S. Postal Service records. Please update your address.";
    }
    else {
        str = @"There is something wrong with your AHA account. Please contact AHA for details";
    }
    UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:@"Account Error"
                                                                    message:str
                                                             preferredStyle:UIAlertControllerStyleAlert];
    [alert2 addAction:[UIAlertAction actionWithTitle:@"Update Info"
                                               style:UIAlertActionStyleCancel
                                             handler:^void (UIAlertAction *action)
                       {
                           UpdateUserViewController *update = [[UpdateUserViewController alloc] initWithStyle:UITableViewStyleGrouped];
                           update.excludeList = @[@"excludedList", @"showPhone"/*, @"firstName", @"lastName", @"prefix", @"phone"*/];
                           UserForm *form = (UserForm *)update.formController.form;
                           form.firstName = @"test";
                           update.validateAddress = YES;
                           update.showCancel = YES;
                           UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:update];
                           nav.modalPresentationStyle = UIModalPresentationFormSheet;
                           [self.navigationController presentViewController:nav animated:YES completion:nil];
                       }]];
    [alert2 addAction:[UIAlertAction actionWithTitle:@"Contact AHA"
                                               style:UIAlertActionStyleDefault
                                             handler:^void (UIAlertAction *action)
                       {
                           //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.aha.org/updateprofile"]];
                           ActionCenterManager *acm = [ActionCenterManager sharedInstance];
                           acm.contacAHA = YES;
                           AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                           [ad openSideMenu];
                           if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                               [[NSNotificationCenter defaultCenter] postNotificationName: @"showContact" object:nil userInfo:nil];
                           }
                       }]];
    [self presentViewController:alert2 animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_viewType == kViewTypeDirectory) {
        
    if (isSearching) {
        return filteredDirectory.count;
    } else {
        return list.count;
        }
    }
    
    if (_viewType == kViewTypeRecipients) {
        return list.count;
    }
    if (_viewType == kViewTypeOfficials) {
        return self.officialsSectionNames.count;
    }
    if (_viewType == kViewTypeByCommittee) {
        return self.committeeSectionNames.count;
    }
    if (_viewType == kViewTypeByLastName) {
        return self.lastNameSectionNames.count;
    }
    /* if (_viewType == kViewTypeByParty) {
        return self.partySectionNames.count;
    } */
    else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_viewType == kViewTypeDirectory) {
        
        NSArray *selectedList;
        if (isSearching) {
            selectedList = filteredDirectory;
        } else {
            selectedList = list;
        }
        VoterVoiceBody *body = (VoterVoiceBody *)selectedList[section];
        return body.matches.count;
    }
    if (_viewType == kViewTypeRecipients) {
        VoterVoiceBody *body = (VoterVoiceBody *)list[section];
        return body.matches.count;
    }
    
    if (_viewType == kViewTypeOfficials) {
        NSString *sectionName = self.officialsSectionNames[section];
        return [self.officialsSectionArrays[sectionName] count];
    }
    
    if (_viewType == kViewTypeByCommittee) {
        NSString *sectionName = self.committeeSectionNames[section];
        return [self.committeeSectionArrays[sectionName] count];
    }
    
    if (_viewType == kViewTypeByLastName) {
        NSString *sectionName = self.lastNameSectionNames[section];
        return [self.lastNameSectionArrays[sectionName] count];
    }
    /* if (_viewType == kViewTypeByParty) {
        NSString *sectionName = self.partySectionNames[section];
        return [self.partySectionArrays[sectionName] count];
    } */
    if (_viewType == kViewTypeFilterMenu) {
        return self.data.count;
    }
    else {
        return list.count;
    }
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (_viewType == kViewTypeDirectory) {
        
        NSArray *selectedList;
        
        if (isSearching) {
            selectedList = filteredDirectory;
        } else {
            selectedList = list;
        }
        
        VoterVoiceBody *body = (VoterVoiceBody *)selectedList[section];
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
        UIView *view = [[UIView alloc] initWithFrame:frame];
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, frame.size.width - 55, 30)];
        NSString *str = body.groupId;
        
        [view setBackgroundColor:[UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f]];
        header.text = [NSString stringWithFormat:@"%@", str];
        [header setFont:[UIFont fontWithName:@"Helvetica-Bold"  size:16.0f]];
        [header setTextColor:[UIColor whiteColor]];
        [view addSubview:header];

        return view;
    }
    
    if (_viewType == kViewTypeRecipients) {
        
        VoterVoiceBody *body = (VoterVoiceBody *)list[section];
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
        UIView *view = [[UIView alloc] initWithFrame:frame];
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, frame.size.width - 55, 30)];
        
        [view setBackgroundColor:[UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f]];
        [header setFont:[UIFont fontWithName:@"Helvetica-Bold"  size:16.0f]];
        [header setTextColor:[UIColor whiteColor]];
        header.text = [body sponsorType];
        [view addSubview:header];
        
        return view;
    }
    
    if (_viewType == kViewTypeOfficials) {
        
        NSString *sectionName = self.officialsSectionNames[section];
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [tableView headerViewForSection:section].frame.size.width, [tableView headerViewForSection:section].frame.size.height)];
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, frame.size.width - 55, 30)];
        
//        if ([sectionName isEqualToString:@"Alabama"]) {
//            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 4, 15, 15)];
//            imageView.image = [UIImage imageNamed:@"Alabama-1"];
//            [v addSubview:imageView];
//        }

        [view setBackgroundColor:[UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f]];
        [header setFont:[UIFont fontWithName:@"Helvetica-Bold"  size:16.0f]];
        [header setTextColor:[UIColor whiteColor]];
        header.text = sectionName;
        [view addSubview:header];
        
        return view;
    }
    if (_viewType == kViewTypeByCommittee) {
        
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [tableView headerViewForSection:section].frame.size.width, [tableView headerViewForSection:section].frame.size.height)];
        
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, frame.size.width - 55, 30)];
        
        [view setBackgroundColor:[UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f]];
        
        [header setFont:[UIFont fontWithName:@"Helvetica-Bold"  size:16.0f]];
        
        [header setTextColor:[UIColor whiteColor]];
        
        NSString *sectionName = self.committeeSectionNames[section];
        
        header.text = sectionName;
        
        [view addSubview:header];
        
        return view;
    }
    if (_viewType == kViewTypeByLastName) {
        
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, 40);

        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [tableView headerViewForSection:section].frame.size.width, [tableView headerViewForSection:section].frame.size.height)];
        
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, frame.size.width - 55, 30)];
        
        [view setBackgroundColor:[UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f]];
        
        [header setFont:[UIFont fontWithName:@"Helvetica-Bold"  size:16.0f]];
        
        [header setTextColor:[UIColor whiteColor]];
        
        NSString *sectionName = self.lastNameSectionNames[section];
        
        header.text = sectionName;
        
        [view addSubview:header];
        
        return view;
    }
    /* if (_viewType == kViewTypeByParty) {
        
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, 40);

        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [tableView headerViewForSection:section].frame.size.width, [tableView headerViewForSection:section].frame.size.height)];
        
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, frame.size.width - 55, 30)];
        
        [view setBackgroundColor:[UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f]];
        
        [header setFont:[UIFont fontWithName:@"Helvetica-Bold"  size:16.0f]];
        
        [header setTextColor:[UIColor whiteColor]];
        
        NSString *sectionName = self.partySectionNames[section];
        
        if ([sectionName isEqualToString:@"D"]) {
            sectionName = @"Democratic";
        }
        if ([sectionName isEqualToString:@"R"]) {
            sectionName = @"Republican";
        }
        if ([sectionName isEqualToString:@"I"]) {
            sectionName = @"Independent";
        }
        
        header.text = sectionName;
        
        [view addSubview:header];
        
        return view;
    } */
    
    else {
        UIView *view = [[UIView alloc] init];
        
        view.backgroundColor = [UIColor clearColor];
        
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if(_viewType == kViewTypeDirectory ||
       _viewType == kViewTypeRecipients||
       _viewType == kViewTypeOfficials ||
       _viewType == kViewTypeByCommittee ||
       _viewType == kViewTypeByLastName) //||
       /* _viewType == kViewTypeByParty) */ {
        
        return 29;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [tableView headerViewForSection:section].frame.size.width, [tableView headerViewForSection:section].frame.size.height)];
    
    return view.frame.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_viewType == kViewTypeTwitter) {
        return 238.0f;
    }
    else if (_viewType == kViewTypeCampaign || _viewType == kViewTypeAHANews) {
        return 110.0f;
    }
    else if (_viewType == kViewTypeContactUs) {
        return 235.0f;
    }
    else if (_viewType == kViewTypeRecipients || _viewType == kViewTypeOfficials || _viewType == kViewTypeDirectory || _viewType == kViewTypeByCommittee || _viewType == kViewTypeByLastName) /*_viewType == kViewTypeByParty)*/ {
        
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Targets"];
        return cell.bounds.size.height;
    
    }
    else {
        return 44.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tableViewSort) {
        
        UITableViewCell *cell = [self.tableViewSort dequeueReusableCellWithIdentifier:@"sortID"];
        
        //self.tableViewSort.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        self.tableViewSort.layer.shadowOpacity = 1.0;
        self.tableViewSort.layer.shadowRadius = 1.7;
        self.tableViewSort.layer.shadowColor = [UIColor blackColor].CGColor;
        self.tableViewSort.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        
        self.tableViewSort.clipsToBounds = NO;
        //self.tableViewSort.layer.masksToBounds = NO;
        //self.tableViewSort.layer.cornerRadius = 5;
        
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.clipsToBounds = YES;

        self.tableViewSort.layer.borderWidth = 1.0;
        tableView.layer.borderColor = ([UIColor colorWithRed:(217.0/255.0) green:217.0/255.0 blue:214.0/255.0 alpha:1.0].CGColor);;
        
        if (cell == nil) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"sortID"];
            
        }
        
        if ([self.title isEqualToString:@"My Directory"]) {
            
            if (indexPath.row == 4) {
                [self.tableViewSort selectRowAtIndexPath:indexPath
                                                animated:TRUE scrollPosition:UITableViewScrollPositionNone];
                
                UIView *colorView = [[UIView alloc] init];
                colorView.backgroundColor = [UIColor colorWithRed:(217.0/255.0) green:217.0/255.0 blue:214.0/255.0 alpha:1.0];
                
                [cell setSelectedBackgroundView:colorView];
                
            }
            
        } else if ([self.title isEqualToString:@"Directory by State"]) {
            
            if (indexPath.row == 3) {
                [self.tableViewSort selectRowAtIndexPath:indexPath
                                                animated:TRUE scrollPosition:UITableViewScrollPositionNone];
                
                UIView *colorView = [[UIView alloc] init];
                colorView.backgroundColor = [UIColor whiteColor];
                
                colorView.backgroundColor = [UIColor colorWithRed:(217.0/255.0) green:217.0/255.0 blue:214.0/255.0 alpha:1.0];
                
                
                [cell setSelectedBackgroundView:colorView];
                
            }
            
        } else if ([self.title isEqualToString:@"Directory by Body"]) {
            
            if (indexPath.row == 0) {
                [self.tableViewSort selectRowAtIndexPath:indexPath
                                                animated:TRUE scrollPosition:UITableViewScrollPositionNone];
                
                UIView *colorView = [[UIView alloc] init];

                colorView.backgroundColor = [UIColor colorWithRed:(217.0/255.0) green:217.0/255.0 blue:214.0/255.0 alpha:1.0];
                [cell setSelectedBackgroundView:colorView];
                
            }
            
        } else if ([self.title isEqualToString:@"Directory by Last Name"]) {
            
            if (indexPath.row == 1) {
                [self.tableViewSort selectRowAtIndexPath:indexPath
                                                animated:TRUE scrollPosition:UITableViewScrollPositionNone];
                
                UIView *colorView = [[UIView alloc] init];
                
                colorView.backgroundColor = [UIColor colorWithRed:(217.0/255.0) green:217.0/255.0 blue:214.0/255.0 alpha:1.0];
                [cell setSelectedBackgroundView:colorView];
                
            }
            
        } else if ([self.title isEqualToString:@"Directory by Party"]) {
            
            if (indexPath.row == 2) {
                [self.tableViewSort selectRowAtIndexPath:indexPath
                                                animated:TRUE scrollPosition:UITableViewScrollPositionNone];
                
                UIView *colorView = [[UIView alloc] init];
                colorView.backgroundColor = [UIColor colorWithRed:(217.0/255.0) green:217.0/255.0 blue:214.0/255.0 alpha:1.0];
                [cell setSelectedBackgroundView:colorView];
            
            }
        }
        
        cell.textLabel.text = [self.data objectAtIndex:indexPath.row];
        cell.textLabel.textColor = [UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f];
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        
        return cell;
    }
    if (_viewType == kViewTypeContactUs) {
        AHAAdvisoryTableViewCell *cell = (AHAAdvisoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"advisories"];
       
        NSDictionary *row = (NSDictionary *)list[indexPath.row];
        [cell setupCellWithTitle:row[@"title"]
                            date:nil
                     description:row[@"text"]];
        cell.advisoryTitleLabel.textAlignment = NSTextAlignmentCenter;
        cell.advisoryDescriptionLabel.textAlignment = NSTextAlignmentCenter;
        
        cell.backgroundColor = [UIColor lightGrayColor];
        return cell;
    }
    if (_viewType == kViewTypeTwitter) {
        TwitterTableViewCell *cell = (TwitterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"twitter_feed_cell"];
        UserStatusTwitterItem *item = [list objectAtIndex:indexPath.row];
        
        cell.nameLabel.text = item.user.name;
        cell.accountNameLabel.text = [NSString stringWithFormat:@"@%@", item.user.screenName];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.tweetLabel setText:[self cleanString:item.text]];
        [cell.tweetLabel setDetectionBlock:^(STTweetHotWord hotWord, NSString *string, NSString *protocol, NSRange range) {
            [self showTwitter:string];
        }];
        
        //CGSize size = [cell.tweetLabel suggestedFrameSizeToFitEntireStringConstraintedToWidth:cell.tweetLabel.frame.size.width];
        CGRect frame = cell.tweetLabel.frame;
        //frame.size.height = size.height;
        cell.tweetLabel.frame = frame;
       
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"eee MMM dd HH:mm:ss ZZZZ yyyy"];
        NSDate *date = [df dateFromString:item.createdAt];
        
        [df setDateFormat:@"MM/dd/yyyy"];
        NSString *dateStr = [df stringFromDate:date];
        cell.dateLabel.text = [ActionCenterManager formatDate:dateStr];;
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
        timeFormatter.dateFormat = @"HH:mm";
        
        NSString *dateString = [timeFormatter stringFromDate: date];
        cell.timeLabel.text = dateString;
        
        cell.backgroundColor = [UIColor lightGrayColor];
        return cell;
    }
    if (_viewType == kViewTypeCampaign) {
        AHAAdvisoryTableViewCell *cell = (AHAAdvisoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"fact_sheets" forIndexPath:indexPath];
        VoterVoiceBody *body = (VoterVoiceBody *)voter.response.body[indexPath.row];
        cell.advisoryTitleLabel.text = body.headline;
        cell.backgroundColor = [UIColor lightGrayColor];
        
        return cell;
    }
    if (_viewType == kViewTypeAHANews) {
        AHAAdvisoryTableViewCell *cell = (AHAAdvisoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"aha_news"];
       
        NSDictionary *row = (NSDictionary *)list[indexPath.row];
        
        [cell setupCellWithTitle:row[@"name"]
                            date:[ActionCenterManager formatDate:row[@"published"]]
                     description:nil];
        
        cell.backgroundColor = [UIColor lightGrayColor];
        return cell;
    }
    if (_viewType == kViewTypeDirectory) {
        
        DirectoryTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Targets" forIndexPath:indexPath];
        
        [cell.nameLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
        [cell.detailLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:16]];
        
        cell.layer.shadowOpacity = 1.0;
        cell.layer.shadowRadius = 1.1;
        cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        cell.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        
        NSArray *selectedList;
        
        if (isSearching) {
            selectedList = filteredDirectory;
        } else {
            selectedList = list;
        }
        
        VoterVoiceBody *body = (VoterVoiceBody *)selectedList[indexPath.section];
        VoterVoiceMatches *match = (VoterVoiceMatches *)body.matches[indexPath.row];
        
        if ([match.name hasPrefix:@"Representative"]) {
            match.name = [match.name stringByReplacingOccurrencesOfString:@"Representative" withString:@"Rep."];

        } if ([match.name hasPrefix:@"Senator"]) {
            match.name = [match.name stringByReplacingOccurrencesOfString:@"Senator" withString:@"Sen."];
        }
        
        self.nameString = match.name;
        cell.nameLabel.text = self.nameString;
        
        [cell.directoryImageView layoutIfNeeded];
        cell.directoryImageView.layer.cornerRadius = cell.directoryImageView.frame.size.height / 2;
        cell.directoryImageView.layer.masksToBounds = YES;
        cell.directoryImageView.layer.borderWidth = 1.5;
        
        [action getProfile:[match.id stringValue] withType:match.type completion:^(NSDictionary *dict, NSError *error) {
            
                if ([[dict valueForKeyPath:@"response.body.displayName"] rangeOfString:@"Democrat"].location == NSNotFound) {
                    
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:157.0f/255.0f green:34.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;
                } else {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f].CGColor;
                }

            NSString *imageString = [dict valueForKeyPath:@"response.body.photoUrl"];
            NSURL *url = [NSURL URLWithString:imageString];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *image = [UIImage imageWithData:data];
            
            NSString *displayName = [dict valueForKeyPath:@"response.body.displayName"];
            NSRange from = [displayName rangeOfString:@"("];
            NSRange to = [displayName rangeOfString:@")"];
            
            NSString *substring = [displayName substringWithRange:NSMakeRange
                                   (from.location + from.length, to.location - from.location - from.length)];
            
            if ([substring rangeOfString:@"Democrat"].location != NSNotFound) {
                
                if ([substring rangeOfString:@"00"].location != NSNotFound) {
                    
                    NSString *endString = [substring stringByReplacingOccurrencesOfString:@"00" withString:@"0"];
                    
                    cell.detailLabel.text = endString;
                } else {
                    cell.detailLabel.text = substring;
                }
                
            } else if ([substring containsString:@"Republican"]) {
                
                if ([substring rangeOfString:@"00"].location != NSNotFound) {
                    
                    NSString *endString = [substring stringByReplacingOccurrencesOfString:@"00" withString:@"0"];
                    
                    cell.detailLabel.text = endString;
                } else {
                    cell.detailLabel.text = substring;
                }
                
            } else if ([substring containsString:@"Independent"]) {
                
                if ([substring rangeOfString:@"00"].location != NSNotFound) {
                    
                    NSString *endString = [substring stringByReplacingOccurrencesOfString:@"00" withString:@"0"];
                    
                    cell.detailLabel.text = endString;
                } else {
                    cell.detailLabel.text = substring;
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.directoryImageView.image = image;
            });
        }];
        
        return cell;
    }
    
    if (_viewType == kViewTypeRecipients) {
        
        DirectoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Targets" forIndexPath:indexPath];
        
        if (!cell) {
            cell = [[DirectoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Targets"];
        }
        
        [cell.nameLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        [cell.detailLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:18]];
        
        cell.layer.shadowOpacity = 1.0;
        cell.layer.shadowRadius = 1.1;
        cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        cell.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        
        VoterVoiceBody *body = (VoterVoiceBody *)list[indexPath.section];
        VoterVoiceMatches *match = (VoterVoiceMatches *)body.matches[indexPath.row];
        cell.nameLabel.text = match.name;
        
        [cell.directoryImageView layoutIfNeeded];
        cell.directoryImageView.layer.cornerRadius = cell.directoryImageView.frame.size.height / 2;
        cell.directoryImageView.layer.masksToBounds = YES;
        cell.directoryImageView.layer.borderWidth = 1.5;
        //cell.messageImageView.image = [UIImage imageNamed:@"arrow@2x"];
        
        [action getProfile:[match.id stringValue] withType:match.type completion:^(NSDictionary *dict, NSError *error) {
            
            NSString *imageString = [dict valueForKeyPath:@"response.body.photoUrl"];
            NSURL *url = [NSURL URLWithString:imageString];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *image = [UIImage imageWithData:data];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([[dict valueForKeyPath:@"response.body.displayName"] rangeOfString:@"Democrat"].location == NSNotFound) {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:157.0f/255.0f green:34.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor; //(AHARed)
                    cell.detailLabel.text = @"Republican";
                } else {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f].CGColor; //(AHABlue)
                    cell.detailLabel.text = @"Democrat";
                }
                cell.directoryImageView.image = image;
            });
        }];
        
        return cell;
    }
    

    if (_viewType == kViewTypeOfficials) {
        
        DirectoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Targets" forIndexPath:indexPath];
        
        if (cell == nil){
            cell =  [[DirectoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:@"Targets"];
        }
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
        
        dispatch_async(queue, ^{
            
            NSString *sectionName = self.officialsSectionNames[indexPath.section];
            NSDictionary *official = self.officialsSectionArrays[sectionName][indexPath.row];
            
            NSString *title = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.title"];
            
            if ([title isEqualToString:@"Senator"]) {
                title = @"Sen.";
            } else if ([title isEqualToString:@"Representative"]) {
                title = @"Rep.";
            } else if ([title isEqualToString:@"Delegate"]) {
                title = @"Del.";
            } else if ([title isEqualToString:@"Majority Leader"]) {
                title = @"Maj. Leader";
            } else if ([title isEqualToString:@"Minority Leader"]) {
                title = @"Min. Leader";
            }
            
            NSString *republican = @"R";
            NSString *democrat = @"D";
            NSString *independant = @"I";
            
            NSString *first = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.givenNames"];
            NSString *last = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.surname"];
            
            NSString *party = (NSString *)[official valueForKeyPath:@"occupancy.politician.party"];
            
            if ([party isEqualToString:republican]) {
                party = @"Republican";
            } else if ([party isEqualToString:democrat]) {
                party = @"Democrat";
            } else if ([party isEqualToString:independant]) {
                party = @"Independant";
            }
            
            NSString *stateAbv = (NSString *)[official valueForKeyPath:@"occupancy.districtOffice.address.state"];
            NSString *dash = @"-";
            NSString *district = (NSString *)[official valueForKeyPath:@"office.electoralDistrict"];
            
            if ([district isKindOfClass:[NSNull class]]) {
                district = @"";
                dash = @"";
            }
            
            if ([district hasPrefix:@"0"]) {
                district = [district substringFromIndex:1];
            }
            
            

            NSString *nameString = [NSString stringWithFormat:@"%@ %@ %@", title, first, last];
            NSString *cityandstate = [NSString stringWithFormat:@"%@-%@%@%@", party, stateAbv, dash, district];
            
            if ([district isKindOfClass:[NSNull class]]) {
                district = @"";
                dash = @"";
            }

            //NSString *districtString = [NSString stringWithFormat:@"%@, %@ %@ %@", city, state, dash, district];
            
            NSString *imageString = (NSString *)[official valueForKeyPath:@"occupancy.politician.photoUrl"];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                if ([party isEqualToString:@"Republican"]) {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:157.0f/255.0f green:34.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;
                }
                else if ([party isEqualToString:@"Democrat"]) {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f].CGColor;
                }
                else if ([party isEqualToString:@"Independant"]){
                    cell.directoryImageView.layer.borderColor = [UIColor grayColor].CGColor;
                }
                
                cell.directoryImageView.layer.cornerRadius = cell.directoryImageView.frame.size.height / 2;
                cell.directoryImageView.layer.masksToBounds = YES;
                cell.directoryImageView.layer.borderWidth = 1.5;
                
                [cell.nameLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
                [cell.detailLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:16]];
                
                cell.layer.shadowOpacity = 1.0;
                cell.layer.shadowRadius = 1.1;
                cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
                cell.layer.shadowOffset = CGSizeMake(0.0, 0.0);
                
                cell.nameLabel.text = nameString;
                cell.detailLabel.text = cityandstate;
                
                if ([imageString isKindOfClass:[NSNull class]]) {
                    
                    NSString *imageName = @"aha_logo";
                    UIImage *image = [UIImage imageNamed:imageName];
                    [cell.directoryImageView setImage:image];
                    
                } else {
                    
                    NSURL *url = [NSURL URLWithString:imageString];
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                           completionHandler:^(NSURLResponse * _Nullable response,
                                                               NSData * _Nullable data,
                                                               NSError * _Nullable connectionError) {
                        if (!connectionError) {
                            UIImage *image = [UIImage imageWithData:data];
                            [cell.directoryImageView setImage:image];
                            
                        }
                    }];
                }
            });
        });
        
        return cell;
    }
                      
    if (_viewType == kViewTypeByCommittee) {
        
        DirectoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Targets" forIndexPath:indexPath];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
                dispatch_async(queue, ^{
            
            NSString *sectionName = self.committeeSectionNames[indexPath.section];
            NSDictionary *official = self.committeeSectionArrays[sectionName][indexPath.row];
                    
                    NSString *title = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.title"];
                    
                    if ([title isEqualToString:@"Senator"]) {
                        title = @"Sen.";
                    } else if ([title isEqualToString:@"Representative"]) {
                        title = @"Rep.";
                    } else if ([title isEqualToString:@"Delegate"]) {
                        title = @"Del.";
                    } else if ([title isEqualToString:@"Majority Leader"]) {
                        title = @"Maj. Leader";
                    } else if ([title isEqualToString:@"Minority Leader"]) {
                        title = @"Min. Leader";
                    }
            
                    NSString *first = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.givenNames"];
                    NSString *last = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.surname"];
                    NSString *party = (NSString *)[official valueForKeyPath:@"occupancy.politician.party"];
                    NSString *stateAbv = (NSString *)[official valueForKeyPath:@"occupancy.districtOffice.address.state"];
                    NSString *dash = @"-";
                    NSString *district = (NSString *)[official valueForKeyPath:@"office.electoralDistrict"];
                    
                    NSString *republican = @"R";
                    NSString *democrat = @"D";
                    NSString *independant = @"I";
                    
                    if ([party isEqualToString:republican]) {
                        party = @"Republican";
                    } else if ([party isEqualToString:democrat]) {
                        party = @"Democrat";
                    } else if ([party isEqualToString:independant]) {
                        party = @"Independant";
                    }
            
                    NSString *imageString = (NSString *)[official valueForKeyPath:@"occupancy.politician.photoUrl"];
                    NSString *nameString = [NSString stringWithFormat:@"%@ %@ %@", title, first, last];
                    
                    if ([district hasPrefix:@"0"]) {
                        district = [district substringFromIndex:1];
                    }
                    
                    if ([district isKindOfClass:[NSNull class]]) {
                        district = @"";
                        dash = @"";
                    }
                    
            NSString *districtString = [NSString stringWithFormat:@"%@-%@%@%@", party, stateAbv, dash, district];
                    
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                [cell.nameLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
                [cell.detailLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:16]];
                
                cell.layer.shadowOpacity = 1.0;
                cell.layer.shadowRadius = 1.1;
                cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
                cell.layer.shadowOffset = CGSizeMake(0.0, 0.0);
                
                cell.nameLabel.text = nameString;
                cell.detailLabel.text = districtString;
                
                if ([imageString isKindOfClass:[NSNull class]]) {
                    NSString *imageName = @"aha_logo";
                    UIImage *image = [UIImage imageNamed:imageName];
                    [cell.directoryImageView setImage:image];
                    
                } {
                    NSURL *url = [NSURL URLWithString:imageString];
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                           completionHandler:^(NSURLResponse * _Nullable response,
                                                               NSData * _Nullable data,
                                                               NSError * _Nullable connectionError) {
                                               if (!connectionError) {
                                                   UIImage *image = [UIImage imageWithData:data];
                                                   [cell.directoryImageView setImage:image];
                                               }
                                           }];
                }
                
                if ([party isEqualToString:@"Republican"]) {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:157.0f/255.0f green:34.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;
                }
                else if ([party isEqualToString:@"Democrat"]) {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f].CGColor;
                }
                else if ([party isEqualToString:@"Independant"]){
                    cell.directoryImageView.layer.borderColor = [UIColor grayColor].CGColor;
                }
                
                cell.directoryImageView.layer.cornerRadius = cell.directoryImageView.frame.size.height / 2;
                cell.directoryImageView.layer.masksToBounds = YES;
                cell.directoryImageView.layer.borderWidth = 1.5;
                
            });
        });
        
        return cell;
    }
    
    if (_viewType == kViewTypeByLastName) {
        
        DirectoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Targets" forIndexPath:indexPath];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
        dispatch_async(queue, ^{
            
            NSString *sectionName = self.lastNameSectionNames[indexPath.section];
            NSDictionary *official = self.lastNameSectionArrays[sectionName][indexPath.row];
            
            NSString *title = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.title"];
            
            if ([title isEqualToString:@"Senator"]) {
                title = @"Sen.";
            } else if ([title isEqualToString:@"Representative"]) {
                title = @"Rep.";
            } else if ([title isEqualToString:@"Delegate"]) {
                title = @"Del.";
            } else if ([title isEqualToString:@"Majority Leader"]) {
                title = @"Maj. Leader";
            } else if ([title isEqualToString:@"Minority Leader"]) {
                title = @"Min. Leader";
            }
            
            NSString *first = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.givenNames"];
            NSString *last = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.surname"];
            NSString *party = (NSString *)[official valueForKeyPath:@"occupancy.politician.party"];
            NSString *district = (NSString *)[official valueForKeyPath:@"office.electoralDistrict"];
            NSString *dash = @"-";
            
            NSString *republican = @"R";
            NSString *democrat = @"D";
            NSString *independant = @"I";
            
            if ([party isEqualToString:republican]) {
                party = @"Republican";
            } else if ([party isEqualToString:democrat]) {
                party = @"Democrat";
            } else if ([party isEqualToString:independant]) {
                party = @"Independant";
            }
            
            if ([district isKindOfClass:[NSNull class]]) {
                district = @"";
                dash = @"";
            }
            
            if ([district hasPrefix:@"0"]) {
                district = [district substringFromIndex:1];
            }
            
            NSString *state = (NSString *)[official valueForKeyPath:@"occupancy.districtOffice.address.state"];
            NSString *nameString = [NSString stringWithFormat:@"%@ %@ %@", title, first, last];

            //NSString *city = (NSString *)[official valueForKeyPath:@"occupancy.districtOffice.address.city"];
            //NSString *officeCity = (NSString *)[official valueForKeyPath:@"occupancy.capitolOffice.address.city"];
            //NSString *officeState = (NSString *)[official valueForKeyPath:@"occupancy.capitolOffice.address.state"];
            
            NSString *imageString = (NSString *)[official valueForKeyPath:@"occupancy.politician.photoUrl"];
            
            if ([district isKindOfClass:[NSNull class]]) {
                district = @"";
                dash = @"";
            }
            
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [cell.nameLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
            [cell.detailLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:16]];
            
            cell.layer.shadowOpacity = 1.0;
            cell.layer.shadowRadius = 1.1;
            cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
            cell.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        
            cell.nameLabel.text = nameString;

                NSString *districtString = [NSString stringWithFormat:@"%@-%@%@%@", party, state, dash, district];
                cell.detailLabel.text = districtString;
            
            if ([party isEqualToString:@"Republican"]) {
                cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:157.0f/255.0f green:34.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;
            }
            else if ([party isEqualToString:@"Democrat"]) {
                cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f].CGColor;
            }
            else if ([party isEqualToString:@"Independant"]){
                cell.directoryImageView.layer.borderColor = [UIColor grayColor].CGColor;
            }
            
            cell.directoryImageView.layer.cornerRadius = cell.directoryImageView.frame.size.height / 2;
            cell.directoryImageView.layer.masksToBounds = YES;
            cell.directoryImageView.layer.borderWidth = 1.5;
            
            if ([imageString isKindOfClass:[NSNull class]]) {
                NSString *imageName = @"aha_logo";
                UIImage *image = [UIImage imageNamed:imageName];
                [cell.directoryImageView setImage:image];
                
            } else {
                NSURL *url = [NSURL URLWithString:imageString];

                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse * _Nullable response,
                                                           NSData * _Nullable data,
                                                           NSError * _Nullable connectionError) {
                                           if (!connectionError) {
                                               UIImage *image = [UIImage imageWithData:data];
                                               [cell.directoryImageView setImage:image];
                                           }
                                       }];
            }
        
        });
        
        });
        
        return cell;
        
    } /* if (_viewType == kViewTypeByParty) {
        
        DirectoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Targets" forIndexPath:indexPath];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
        dispatch_async(queue, ^{
            
            NSString *sectionName = self.partySectionNames[indexPath.section];
            NSDictionary *official = self.partySectionArrays[sectionName][indexPath.row];
            NSString *title = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.title"];
            
            if ([title isEqualToString:@"Senator"]) {
                title = @"Sen.";
            } else if ([title isEqualToString:@"Representative"]) {
                title = @"Rep.";
            } else if ([title isEqualToString:@"Delegate"]) {
                title = @"Del.";
            } else if ([title isEqualToString:@"Majority Leader"]) {
                title = @"Maj. Leader";
            } else if ([title isEqualToString:@"Minority Leader"]) {
                title = @"Min. Leader";
            }
            
            NSString *first = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.givenNames"];
            NSString *last = (NSString *)[official valueForKeyPath:@"occupancy.politician.personName.surname"];
            NSString *party = (NSString *)[official valueForKeyPath:@"occupancy.politician.party"];
            NSString *state = (NSString *)[official valueForKeyPath:@"occupancy.districtOffice.address.state"];
            NSString *district = (NSString *)[official valueForKeyPath:@"office.electoralDistrict"];
            NSString *dash = @"-";
            
            NSString *republican = @"R";
            NSString *democrat = @"D";
            NSString *independant = @"I";
            
            if ([party isEqualToString:republican]) {
                party = @"Republican";
            } else if ([party isEqualToString:democrat]) {
                party = @"Democrat";
            } else if ([party isEqualToString:independant]) {
                party = @"Independant";
            }
            
            if ([district isKindOfClass:[NSNull class]]) {
                district = @"";
                dash = @"";
            }
            
            if ([district hasPrefix:@"0"]) {
                district = [district substringFromIndex:1];
            }
            
            NSString *nameString = [NSString stringWithFormat:@"%@ %@ %@", title, first, last];
            NSString *imageString = (NSString *)[official valueForKeyPath:@"occupancy.politician.photoUrl"];
   
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                [cell.nameLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
                [cell.detailLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:16]];
                
                cell.layer.shadowOpacity = 1.0;
                cell.layer.shadowRadius = 1.1;
                cell.layer.shadowColor = [UIColor lightGrayColor].CGColor;
                cell.layer.shadowOffset = CGSizeMake(0.0, 0.0);
                
                cell.nameLabel.text = nameString;

                NSString *districtString = [NSString stringWithFormat:@"%@-%@%@%@", party, state, dash, district];
                cell.detailLabel.text = districtString;
                
                if ([imageString isKindOfClass:[NSNull class]]) {
                    NSString *imageName = @"aha_logo";
                    UIImage *image = [UIImage imageNamed:imageName];
                    [cell.directoryImageView setImage:image];
                    
                } else {
                    NSURL *url = [NSURL URLWithString:imageString];
                    NSURLRequest *request = [NSURLRequest requestWithURL:url];
                    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                                           completionHandler:^(NSURLResponse * _Nullable response,
                                                               NSData * _Nullable data,
                                                               NSError * _Nullable connectionError) {
                                               if (!connectionError) {
                                                   UIImage *image = [UIImage imageWithData:data];
                                                   [cell.directoryImageView setImage:image];
                                               }
                                           }];
                }
                
                if ([party isEqualToString:@"Republican"]) {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:157.0f/255.0f green:34.0f/255.0f blue:53.0f/255.0f alpha:1.0f].CGColor;
                }
                else if ([party isEqualToString:@"Democrat"]) {
                    cell.directoryImageView.layer.borderColor = [UIColor colorWithRed:13.0f/255.0f green:71.0f/255.0f blue:161.0f/255.0f alpha:1.0f].CGColor;
                }
                else if ([party isEqualToString:@"Independant"]){
                    cell.directoryImageView.layer.borderColor = [UIColor grayColor].CGColor;
                }
                
                cell.directoryImageView.layer.cornerRadius = cell.directoryImageView.frame.size.height / 2;
                cell.directoryImageView.layer.masksToBounds = YES;
                cell.directoryImageView.layer.borderWidth = 1.5;
                
            });
        });
        
        return cell;
    } */

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    if (_viewType == kViewTypeTwitter) {
        
    } else if (_viewType == kViewTypeCampaign) {
        
            OAM *oam;
            VoterVoiceBody *body = (VoterVoiceBody *)voter.response.body[indexPath.row];
            CampaignDetailView *detailView = [[CampaignDetailView alloc] initWithFrame:CGRectMake(0, 0, 280, 400)];
        
            [detailView setHeader:body.headline];
            [detailView loadHTMLString:body.alert];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        if (/*[prefs boolForKey:@"inVoterVoice"] == NO*/YES) {
            
            NSString *prefix = [prefs stringForKey:@"prefix"];
            NSString *phone = [prefs stringForKey:@"phone"];
            NSString *address = [prefs stringForKey:@"address"];
            NSString *city = [prefs stringForKey:@"city"];
            NSString *state = [prefs stringForKey:@"state"];
            NSString *zip = [prefs stringForKey:@"zip"];
            
            zip = [zip substringToIndex:5];
            oam = [[OAM alloc] initWithJSONData:[prefs objectForKey:@"user"]];
            
            if (address == nil) {
                [self requiredInfo];
                return;
            
            } else {
                
                detailView.sendButtonTapped = ^(){
                    
                    GeneralTableViewController *gtvc = (GeneralTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"general"];
                    
                    gtvc.viewType = kViewTypeRecipients;
                    gtvc.viewShouldRefresh = YES;
                    gtvc.campaignID = [body.id stringValue];
                    
                    [action verifyAddress:address withZip:zip andCountry:@"US" completion:^(NSDictionary *dict, NSError *err) {
                        
                        NSArray *arr = (NSArray *)[dict valueForKeyPath:@"response.body.addresses"];
                        
                        if ([dict valueForKeyPath:@"response.body.suggestedZipCode"] == [NSNull null] &&
                            [dict valueForKeyPath:@"response.body.message"] == [NSNull null] &&
                            arr.count > 0) {
                            NSDictionary *d = arr[0];
                            [prefs setObject:d[@"streetAddress"] forKey:@"address"];
                            [prefs setObject:d[@"city"] forKey:@"city"];
                            [prefs setObject:d[@"state"] forKey:@"state"];
                            [prefs setObject:d[@"zipCode"] forKey:@"zip"];
                            [prefs synchronize];
                            
                            oam.address_line = address;
                            oam.city = city;
                            oam.state = state;
                            oam.zip = zip;
                            oam.phone = phone;
                            oam.prefix = prefix;
                            
                            [self.navigationController pushViewController:gtvc animated:YES];
                        }
                        else {
                            
                            [self showAlert:@"address" withMessage:@""];
                            
                        }
                    }];
                    
                    [[KGModal sharedInstance] hideAnimated:YES withCompletionBlock:^(){
                        
                    }];
                };
                [[KGModal sharedInstance] showWithContentView:detailView andAnimated:YES];
            }
        }
    }
    else if (_viewType == kViewTypeContactUs) {
        if (indexPath.row == 1) {
            [self campaignFeedbackPressed:nil];
        }
        if (indexPath.row == 0) {
            CampaignDetailViewController *vc = (CampaignDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"campaignDetail"];
            vc.campaignID = nil;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else if (_viewType == kViewTypeAHANews) {
        NSDictionary *row = (NSDictionary *)list[indexPath.row];
        CampaignDetailView *detailView = [[CampaignDetailView alloc] initWithFrame:CGRectMake(0, 0, 280, 400)];
        [detailView setHeader:row[@"name"]];
        [action getAHAArticle:row[@"link"] completion:^(NSString *response, NSError *err) {
            [detailView loadHTMLString:response];
        }];
        [detailView setButtonTitle:@"Close"];
        detailView.sendButtonTapped = ^(){
            [[KGModal sharedInstance] hideAnimated:YES withCompletionBlock:^(){
            }];
        };
        [[KGModal sharedInstance] showWithContentView:detailView andAnimated:YES];
    }
    else if (_viewType == kViewTypeDirectory) {
        
        [hud showHUDWithMessage:@"Getting Profile"];
        
        if (isSearching) {
            self.selectedList = filteredDirectory;
            
        } else {
            self.selectedList = list;
        }
        
        VoterVoiceBody *body = (VoterVoiceBody *)self.selectedList[indexPath.section];
        VoterVoiceMatches *match = (VoterVoiceMatches *)body.matches[indexPath.row];
        
        ProfileViewController *profileViewController = [[ProfileViewController alloc] init];
        profileViewController.selectedList = self.selectedList;
        
        [action getProfile:[match.id stringValue] withType:match.type completion:^(NSDictionary *d, NSError *error) {
            
            [hud showHUDSucces:YES withMessage:@"Complete"];
            
            ProfileViewController *profileViewController = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
            NSString *nameString = [d valueForKeyPath:@"response.body.displayName"];
            NSRange r1 = [nameString rangeOfString:@"("];
            NSRange r2 = [nameString rangeOfString:@")"];
            
            NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
            NSString *titleString = [nameString substringWithRange:rSub];
            
            nameString = [nameString stringByReplacingOccurrencesOfString:titleString withString:@""];
            nameString = [nameString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            nameString = [nameString stringByReplacingOccurrencesOfString:@")" withString:@""];
            
            profileViewController.nameString = nameString;
            profileViewController.dictionary = d;
            
            [self.navigationController pushViewController:profileViewController animated:YES];
        }];
}
    
    else if (_viewType ==kViewTypeOfficials) {
        
        NSString *sectionName = self.officialsSectionNames[indexPath.section];
        NSDictionary *official = self.officialsSectionArrays[sectionName][indexPath.row];
        NSString *id = (NSString *)[official valueForKeyPath:@"occupancy.id"];
        
        [hud showHUDWithMessage:@"Getting Profile"];
        
        [action getProfile:id withType:@"E" completion:^(NSDictionary *dict, NSError *error) {
            
            [hud showHUDSucces:YES withMessage:@"Complete"];
            
            ProfileViewController *profileViewController = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
            NSString *nameString = [dict valueForKeyPath:@"response.body.displayName"];
            NSRange r1 = [nameString rangeOfString:@"("];
            NSRange r2 = [nameString rangeOfString:@")"];
            
            NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
            NSString *titleString = [nameString substringWithRange:rSub];
            
            nameString = [nameString stringByReplacingOccurrencesOfString:titleString withString:@""];
            nameString = [nameString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            nameString = [nameString stringByReplacingOccurrencesOfString:@")" withString:@""];
            
            profileViewController.nameString = nameString;
            profileViewController.dictionary = dict;
            
            [self.navigationController pushViewController:profileViewController animated:YES];
        }];
    }
    
    else if (_viewType == kViewTypeByCommittee) {
        
        NSString *sectionName = self.committeeSectionNames[indexPath.section];
        NSDictionary *official = self.committeeSectionArrays[sectionName][indexPath.row];
        NSString *id = (NSString *)[official valueForKeyPath:@"occupancy.id"];
        
        [hud showHUDWithMessage:@"Getting Profile"];
        
        [action getProfile:id withType:@"E" completion:^(NSDictionary *dict, NSError *error) {
            
            [hud showHUDSucces:YES withMessage:@"Complete"];
            
            ProfileViewController *profileViewController = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
            NSString *nameString = [dict valueForKeyPath:@"response.body.displayName"];
            NSRange r1 = [nameString rangeOfString:@"("];
            NSRange r2 = [nameString rangeOfString:@")"];
            
            NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
            NSString *titleString = [nameString substringWithRange:rSub];
            
            nameString = [nameString stringByReplacingOccurrencesOfString:titleString withString:@""];
            nameString = [nameString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            nameString = [nameString stringByReplacingOccurrencesOfString:@")" withString:@""];
            
            profileViewController.nameString = nameString;
            profileViewController.dictionary = dict;
            
            [self.navigationController pushViewController:profileViewController animated:YES];
        }];
    }
    
    else if (_viewType == kViewTypeByLastName) {
        
        NSString *sectionName = self.lastNameSectionNames[indexPath.section];
        NSDictionary *official = self.lastNameSectionArrays[sectionName][indexPath.row];
        NSString *id = (NSString *)[official valueForKeyPath:@"occupancy.id"];
        
        [hud showHUDWithMessage:@"Getting Profile"];
        
            [action getProfile:id withType:@"E" completion:^(NSDictionary *dict, NSError *error) {
                
                [hud showHUDSucces:YES withMessage:@"Complete"];
                
                ProfileViewController *profileViewController = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
                
                NSString *nameString = [dict valueForKeyPath:@"response.body.displayName"];
                NSRange r1 = [nameString rangeOfString:@"("];
                NSRange r2 = [nameString rangeOfString:@")"];
                
                NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
                NSString *titleString = [nameString substringWithRange:rSub];
                
                nameString = [nameString stringByReplacingOccurrencesOfString:titleString withString:@""];
                nameString = [nameString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                nameString = [nameString stringByReplacingOccurrencesOfString:@")" withString:@""];
                
                profileViewController.nameString = nameString;
                profileViewController.dictionary = dict;
                
                [self.navigationController pushViewController:profileViewController animated:YES];
        }];
    }
    
     /* else if (_viewType == kViewTypeByParty) {

        NSString *sectionName = self.partySectionNames[indexPath.section];
        NSDictionary *official = self.partySectionArrays[sectionName][indexPath.row];
        NSString *id = (NSString *)[official valueForKeyPath:@"occupancy.id"];
        
        [hud showHUDWithMessage:@"Getting Profile"];
        
        [action getProfile:id withType:@"E" completion:^(NSDictionary *dict, NSError *error) {
            [hud showHUDSucces:YES withMessage:@"Complete"];
            
            ProfileViewController *profileViewController = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            
            NSString *nameString = [dict valueForKeyPath:@"response.body.displayName"];
            NSRange r1 = [nameString rangeOfString:@"("];
            NSRange r2 = [nameString rangeOfString:@")"];
            
            NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
            NSString *titleString = [nameString substringWithRange:rSub];
            
            nameString = [nameString stringByReplacingOccurrencesOfString:titleString withString:@""];
            nameString = [nameString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            nameString = [nameString stringByReplacingOccurrencesOfString:@")" withString:@""];
            
            profileViewController.nameString = nameString;
            profileViewController.dictionary = dict;
            
            [self.navigationController pushViewController:profileViewController animated:YES];

            
        }];
    } */
    
    else if (_viewType == kViewTypeRecipients) {
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        if (YES) {
            
            CampaignDetailViewController *cdvc = (CampaignDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"campaignDetail"];
            
            VoterVoiceBody *body = (VoterVoiceBody *)list[indexPath.section];
            VoterVoiceMatches *match = (VoterVoiceMatches *)body.matches[indexPath.row];
            
            NSString *groupID = body.groupId;
            
            cdvc.bodies = list;
            cdvc.campaignID = _campaignID;
            cdvc.groupID = groupID;
            cdvc.currentMatch = match;
            cdvc.currentIndex = indexPath.row;
            
            [self.navigationController pushViewController:cdvc animated:YES];
        
        } else {
            
        }
    }
    
    else if (_viewType == kViewTypeFilterMenu) {
        
        UITableViewCell *cell = [self.tableViewSort cellForRowAtIndexPath:indexPath];

        if ([cell.textLabel.text isEqualToString:@"Filter by State"]) {
            self.title = @"Directory by State";
            _viewType = kViewTypeOfficials;
            [self loadCustomView];
        
        } if ([cell.textLabel.text isEqualToString:@"My Directory"]) {
            self.title = @"My Directory";
            _viewType = kViewTypeDirectory;
            _viewShouldRefresh = YES;
            [self loadCustomView];
            
        } if ([cell.textLabel.text isEqualToString:@"Filter by Body"]) {
            self.title = @"Directory by Body";
            _viewType = kViewTypeByCommittee;
            [self loadCustomView];
            
        } if ([cell.textLabel.text isEqualToString:@"Filter by Last Name"]) {
            self.title = @"Directory by Last Name";
            _viewType = kViewTypeByLastName;
            [self loadCustomView];
        
        }  /* if ([cell.textLabel.text isEqualToString:@"Filter by Party"]) {
            self.title = @"Directory by Party";
            _viewType = kViewTypeByParty;
            [self loadCustomView];
        } */
        
        self.tableViewSort.hidden = YES;
        self.menuArrow.hidden = YES;
        self.tableView.scrollEnabled = YES;
        self.tableView.allowsSelection = YES;
    }
}

- (IBAction)officialsButtonPressed {
    
   /* UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PopOverTableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"Pop"];
    controller.modalPresentationStyle = UIModalPresentationNone;
    [self presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    popController.barButtonItem = self.navigationItem.rightBarButtonItem;
    popController.delegate = (id)self; */
    
/*
    // Present the view controller using the popover style.
    pop.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:myPopoverViewController animated: YES completion: nil];
    
    // Get the popover presentation controller and configure it.
    UIPopoverPresentationController *presentationController = [myPopoverViewController popoverPresentationController];
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight;
    presentationController.sourceView = myView;
    presentationController.sourceRect = sourceRect;
  */  
    
    
   /* UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    if ([EventService associatedEventWithMeeting:self.meeting]) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Already in Calendar" style:UIAlertActionStyleDefault handler:nil];
        action.enabled = NO;
        [actionSheet addAction:action];
    } else {
        if ([self.meeting.status isEqualToString:kMeetingStatusConfirmed]) {
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add to Calendar" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self saveEvent];
            }]];
        } else {
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"Not Confirmed yet" style:UIAlertActionStyleDefault handler:nil];
            action.enabled = NO;
            [actionSheet addAction:action];
        }
    }
    
    [self presentViewController:actionSheet animated:YES completion:nil]; */

    

    if (self.tableViewSort.hidden == YES) {
        
        _viewType = kViewTypeFilterMenu;
        [self loadCustomView];
        
        self.tableViewSort.hidden = NO;
        self.menuArrow.hidden = NO;
        self.tableView.scrollEnabled = NO;
        self.tableView.allowsSelection = NO;
    
    } else if (self.tableViewSort.hidden == NO) {

        self.tableViewSort.hidden = YES;
        self.menuArrow.hidden = YES;
        self.tableView.scrollEnabled = YES;
        self.tableView.allowsSelection = YES;

        if ([self.title isEqualToString:@"My Directory"]) {
            _viewType = kViewTypeDirectory;
        }
        if ([self.title isEqualToString:@"Directory by State"]) {
            _viewType = kViewTypeOfficials;
        }
        if ([self.title isEqualToString:@"Directory by Body"]) {
            _viewType = kViewTypeByCommittee;
        }
        if ([self.title isEqualToString:@"Directory by Last Name"]) {
            _viewType = kViewTypeByLastName;
        }
        /* if ([self.title isEqualToString:@"Directory by Party"]) {
            _viewType = kViewTypeByParty;
        } */
    }
}

- (NSArray *)officialsSectionNames {
    
    if (_officialsSectionNames == nil) {
        [self generateSectionsFromList:list];
    }
    return _officialsSectionNames;
}

- (NSArray *)committeeSectionNames {
    
    if (_committeeSectionNames == nil) {
        [self generateSectionsFromList:list];
    }
    return _committeeSectionNames;
}

- (NSArray *)lastNameSectionNames {
    
    if (_lastNameSectionNames == nil) {
        [self generateSectionsFromList:list];
    }
    return _lastNameSectionNames;
}

- (NSArray *)partySectionNames {
    
    if (_partySectionNames == nil) {
        [self generateSectionsFromList:list];
    }
    return _partySectionNames;
}
- (void)generateSectionsFromList: (NSArray *)inputList

{
    NSMutableSet *sectionNames = [NSMutableSet set];
    
    // Dictionary of mutable arrays
    NSMutableDictionary *sectionArrays = [NSMutableDictionary dictionary];
    
    if (_viewType == kViewTypeOfficials) {
        for (NSDictionary *item in inputList) {
            
            // Get section name
            NSString *sectionName = item[@"office"][@"delegateGovernment"][@"name"];
            if (sectionName == nil) {
                sectionName = @"N/A";
            }
            [sectionNames addObject:sectionName];
            
            // Get/Create the array for rows
            NSMutableArray *rows = sectionArrays[sectionName];
            if (rows == nil) {
                rows = [NSMutableArray array];
                sectionArrays[sectionName] = rows;
            }
            
            [rows addObject:item];
        }
        
        self.officialsSectionArrays = sectionArrays;
        _officialsSectionNames = [sectionNames.allObjects sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
            return [obj1 compare:obj2];
        }];
    }
    
    else if (_viewType == kViewTypeByCommittee) {
        for (NSDictionary *item in inputList) {
            
            // Get section name
            NSString *sectionName = item[@"office"][@"electedBody"];
            
            if ([sectionName isEqualToString:@"US House"]) {
                sectionName = @"U.S. House of Representatives"; }
            
            if ([sectionName isEqualToString:@"US Senate"]) {
                sectionName = @"U.S. Senate"; }
            
            if ([sectionName isEqualToString:@"President"] ||
                [sectionName isEqualToString:@"Vice President"]) {
                [sectionNames removeObject:sectionName]; }
            
            else {
                [sectionNames addObject:sectionName];
            }
            
            NSMutableArray *rows = sectionArrays[sectionName];
            
            if (rows == nil) {
                rows = [NSMutableArray array];
                sectionArrays[sectionName] = rows;
            }
            
            if ([sectionName isEqualToString:@"President"] ||
                [sectionName isEqualToString:@"Vice President"]) {
                [rows removeObject:item];
                
            } else {
                
                [rows addObject:item];
            }
        }
        
        self.committeeSectionArrays = sectionArrays;
        
        _committeeSectionNames = [sectionNames.allObjects sortedArrayUsingComparator: ^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
            return [obj1 compare:obj2];
            
        }];
        
    }
        else if (_viewType == kViewTypeByLastName) {
            
            for (NSDictionary *item in inputList) {
                
                NSString *sectionName = item[@"occupancy"][@"politician"][@"personName"][@"surname"];
                NSString *firstLetter = [sectionName substringToIndex:1];
                firstLetter = [firstLetter uppercaseString];
                
                if (firstLetter == nil) {
                    firstLetter = @"N/A";
                }
                [sectionNames addObject:firstLetter];
                
                NSMutableArray *rows = sectionArrays[firstLetter];
                
                if (rows == nil) {
                    rows = [NSMutableArray array];
                    sectionArrays[firstLetter] = rows;
                }
                
                [rows addObject:item];
            }
            
            self.lastNameSectionArrays = sectionArrays;
            _lastNameSectionNames = [sectionNames.allObjects sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
                return [obj1 compare:obj2];
            }];
        }
    
    /* if (_viewType == kViewTypeByParty) {
        for (NSDictionary *item in inputList) {
            
            // Get section name
            NSString *sectionName = item[@"occupancy"][@"politician"][@"party"];
            if (sectionName == nil) {
                sectionName = @"N/A";
            }
            [sectionNames addObject:sectionName];
            
            // Get/Create the array for rows
            NSMutableArray *rows = sectionArrays[sectionName];
            if (rows == nil) {
                rows = [NSMutableArray array];
                sectionArrays[sectionName] = rows;
            }
            
            [rows addObject:item];
        }
        
        self.partySectionArrays = sectionArrays;
        _partySectionNames = [sectionNames.allObjects sortedArrayUsingComparator:^NSComparisonResult(NSString *  _Nonnull obj1, NSString *  _Nonnull obj2) {
            return [obj1 compare:obj2];
        }];
    } */

}


-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];

    self.tableView.allowsSelection = YES;
    self.tableView.scrollEnabled = YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    searchBar.text = @"";
    
    if (_viewType == kViewTypeDirectory) {
        
        if ([searchBar.text length] == 0) {
            isSearching = NO;
        }
        [self.tableView reloadData];
    }
    if (_viewType == kViewTypeOfficials) {
        
        if ([searchBar.text length] == 0) {
            [self generateSectionsFromList:list];
        }
        [self.tableView reloadData];
    }
    if (_viewType == kViewTypeByCommittee) {
        
        if ([searchBar.text length] == 0) {
            [self generateSectionsFromList:list];
        }
        [self.tableView reloadData];
    }
    if (_viewType == kViewTypeByLastName) {
        
        if ([searchBar.text length] == 0) {
            [self generateSectionsFromList:list];
        }
        [self.tableView reloadData];
    }
    /* if (_viewType == kViewTypeByParty) {
        
        if ([searchBar.text length] == 0) {
            [self generateSectionsFromList:list];
        }
        [self.tableView reloadData];
    } */
    
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    
    self.tableView.scrollEnabled = YES;
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (_viewType == kViewTypeDirectory) {
        
        if (searchText.length == 0) {
            
            isSearching = NO;
            
        } else {
            
            isSearching = YES;
            filteredDirectory = [[NSMutableArray alloc] init];
            
            for (VoterVoiceBody *body in list) {
                NSMutableArray *matches = [NSMutableArray array];
                
                for (VoterVoiceMatches *match in body.matches) {
                    
                    if ([match.name rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound) {
                        [matches addObject:match];
                    }
                }
                
                if (matches.count > 0) {

                    VoterVoiceBody *vVBody = [[VoterVoiceBody alloc] init];
                    vVBody.groupId = body.groupId;
                    vVBody.matches = matches;
                    [filteredDirectory addObject:vVBody];
                }
            }
        }
        [self.tableView reloadData];
    }
 
    if (_viewType == kViewTypeOfficials) {
    
        if (searchText.length == 0) {
        
            [self generateSectionsFromList:list];
    
        } else {
        
            NSMutableArray *filteredOfficials = [[NSMutableArray alloc] init];

            for (NSDictionary *item in list) {
                
                NSString *firstName = [item valueForKeyPath:@"occupancy.politician.personName.givenNames"];
                NSString *lastName = [item valueForKeyPath:@"occupancy.politician.personName.surname"];
                NSRange firstNameRange = [firstName rangeOfString:searchText options:NSCaseInsensitiveSearch];
                NSRange lastNameRange = [lastName rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
                if ((lastNameRange.location != NSNotFound) || (firstNameRange.location != NSNotFound))
            
                {
                    [filteredOfficials addObject:item];
                }
            }
            [self generateSectionsFromList:filteredOfficials];
        }
        [self.tableView reloadData];
    
    }
    
    if (_viewType == kViewTypeByCommittee) {
        
        if (searchText.length == 0) {
            
            [self generateSectionsFromList:list];
            
        } else {
            
            NSMutableArray *filteredCommittees = [[NSMutableArray alloc] init];
            
            for (NSDictionary *item in list) {
                
                NSString *firstName = [item valueForKeyPath:@"occupancy.politician.personName.givenNames"];
                NSString *lastName = [item valueForKeyPath:@"occupancy.politician.personName.surname"];
                NSRange firstNameRange = [firstName rangeOfString:searchText options:NSCaseInsensitiveSearch];
                NSRange lastNameRange = [lastName rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if ((lastNameRange.location != NSNotFound) || (firstNameRange.location != NSNotFound))
                    
                {
                    [filteredCommittees addObject:item];
                }
            }
            [self generateSectionsFromList:filteredCommittees];
        }
        [self.tableView reloadData];
    }
    
    if (_viewType == kViewTypeByLastName) {
        
        if (searchText.length == 0) {
            
            [self generateSectionsFromList:list];
            
        } else {
            
            NSMutableArray *filteredCommittees = [[NSMutableArray alloc] init];
            
            for (NSDictionary *item in list) {
                
                NSString *firstName = [item valueForKeyPath:@"occupancy.politician.personName.givenNames"];
                NSString *lastName = [item valueForKeyPath:@"occupancy.politician.personName.surname"];
                NSRange firstNameRange = [firstName rangeOfString:searchText options:NSCaseInsensitiveSearch];
                NSRange lastNameRange = [lastName rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if ((lastNameRange.location != NSNotFound) || (firstNameRange.location != NSNotFound))
                    
                {
                    [filteredCommittees addObject:item];
                }
            }
            [self generateSectionsFromList:filteredCommittees];
        }
        [self.tableView reloadData];
    }
    /* if (_viewType == kViewTypeByParty) {
        
        if (searchText.length == 0) {
            
            [self generateSectionsFromList:list];
            
        } else {
            
            NSMutableArray *filteredCommittees = [[NSMutableArray alloc] init];
            
            for (NSDictionary *item in list) {
                
                NSString *firstName = [item valueForKeyPath:@"occupancy.politician.personName.givenNames"];
                NSString *lastName = [item valueForKeyPath:@"occupancy.politician.personName.surname"];
                NSRange firstNameRange = [firstName rangeOfString:searchText options:NSCaseInsensitiveSearch];
                NSRange lastNameRange = [lastName rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if ((lastNameRange.location != NSNotFound) || (firstNameRange.location != NSNotFound))
                    
                {
                    [filteredCommittees addObject:item];
                }
            }
            [self generateSectionsFromList:filteredCommittees];
        }
        [self.tableView reloadData];
    } */
}





@end
