//
//  MainViewController.m
//  ahaactioncenter
//
//  Created by Server on 4/5/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import "MainViewController.h"
#import "LoginViewController.h"
#import "FontAwesomeKit.h"
#import "ActionCenterManager.h"
#import "ProgressHUD.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "UpdateUserViewController.h"
#import "PDFKBasicPDFViewer.h"
#import "PDFKDocument.h"
#import <MediaPlayer/MediaPlayer.h>
#import "MainTableViewCell.h"
#import "CampaignInfoViewController.h"
#import <Haneke/Haneke.h>
#import "AHANews.h"
#import "HTMLParser.h"
#import "AHANewsViewController.h"

@interface MainViewController () <UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate>

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@property (nonatomic, retain) AHANews *news;
@property (nonatomic, retain) NSMutableArray *articles;

@property (nonatomic, retain) NSMutableArray *subCat;
@property (nonatomic, strong) NSMutableDictionary *subCatDict;


@property (nonatomic, strong) NSXMLParser *xmlParser;
@property (nonatomic, strong) NSMutableDictionary *dictTempDataStorage;
@property (nonatomic, strong) NSMutableString *foundValue;
@property (nonatomic, strong) NSString *currentElement;


@property NSArray *campaignList;

@property ActionCenterManager *action;
@property VoterVoice *voter;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.articles = [[NSMutableArray alloc] init];
    
    self.kContentType = kContentTypeNews;
    [self getAHANews];
    
    [self underline:self.newsButton withColor:kAHABlue];
    [self.newsButton setTintColor:kAHABlue];
    [self.campaignsButton setTintColor:kAHABlue];

    self.action = [ActionCenterManager sharedInstance];
    self.voter = [[VoterVoice alloc] init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    self.campaignList = [prefs objectForKey:@"campaignList"];
    self.voter = [prefs objectForKey:@"campaignListVoter"];
    
    //A delay was added to allow the reachability manager to catch up.
    [self performSelector:@selector(getCampaignSummaries) withObject:nil afterDelay:0.5];
    
   // dispatch_async(dispatch_get_main_queue(), ^{
       // [self getCampaignSummaries];
    // });

    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigation_icon"]];
    
    self.mainLabel.textColor = kAHABlue;
    //self.bgImage.image = [UIImage imageNamed:@"capital_white_fade_bg.jpg"];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.navigationItem.leftBarButtonItem = [ActionCenterManager dragButton];
    }
    else {
        self.navigationItem.leftBarButtonItem = [ActionCenterManager splitButton];
    }
    
    FAKFontAwesome *question = [FAKFontAwesome iconWithCode:@"\uf059" size:30];
    [question addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
        
    //Observe for article updates
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(refreshFeed) name:@"articleUpdates" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if (![prefs boolForKey:@"isLoggedIn"])
    {
        LoginViewController *vc = (LoginViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"login"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        nav.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:nav animated:YES completion:nil];
    }
    else {
        //OAM *oam = [[OAM alloc] initWithJSONData:[prefs dataForKey:@"user"]];
        //NSLog(@"oam %@", oam.first_name);
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    // Do view manipulation here.
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

- (void)showTutorial:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"View Tutorial"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"NO"
                                              style:UIAlertActionStyleCancel
                                            handler:^void (UIAlertAction *action)
                      {

                      }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"YES"
                                              style:UIAlertActionStyleDefault
                                            handler:^void (UIAlertAction *action)
                      {
                          NSURL *url = [NSURL URLWithString:
                                        @"https://aha.box.com/shared/static/1riee2bbvgsr6iox003xv4l9b5546275.mp4"];
                          MPMoviePlayerViewController *c = [[MPMoviePlayerViewController alloc]
                                                            initWithContentURL:url];
                          
                          [self.navigationController presentMoviePlayerViewControllerAnimated:c];
                          
                      }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)refresh:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Refresh Content"
                                                                   message:@"Are you sure?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"NO"
                                              style:UIAlertActionStyleCancel
                                            handler:^void (UIAlertAction *action)
                      {
                          /*
                           UpdateUserViewController *update = [[UpdateUserViewController alloc] initWithStyle:UITableViewStyleGrouped];
                           UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:update];
                           nav.modalPresentationStyle = UIModalPresentationFormSheet;
                           [self.navigationController presentViewController:nav animated:YES completion:nil];
                           */
                           /*
                          //Load the document
                          dispatch_async(dispatch_get_main_queue(), ^{
                              PDFKBasicPDFViewer *viewer = [[PDFKBasicPDFViewer alloc] init];
                              NSString *path = [[NSBundle mainBundle] pathForResource:@"Wikipedia" ofType:@"pdf"];
                              PDFKDocument *document = [PDFKDocument documentWithContentsOfFile:path password:@""];
                              NSLog(@"path %@", path);
                              [self.navigationController pushViewController:viewer animated:YES];
                              //[viewer loadDocument:document];
                              [viewer performSelector:@selector(loadDocument:) withObject:document afterDelay:3];
                            
                          });*/
                      }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"YES"
                                              style:UIAlertActionStyleDefault
                                            handler:^void (UIAlertAction *action)
                      {
                          ProgressHUD *hud = [ProgressHUD sharedInstance];
                          [hud showHUDWithMessage:@"Refreshing Content"];
                          //MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                          //hud.label.text = NSLocalizedString(@"Loading...", @"HUD Loading");
                          
                          ActionCenterManager *action2 = [ActionCenterManager sharedInstance];
                          [action2 getAHAFeed:^(NSArray *feeds, NSArray *alerts, NSError *error){
                              //NSLog(@"Feed %@", feeds);
                              [hud showHUDSucces:YES withMessage:@"Content Refreshed"];
                              //hud.label.text = NSLocalizedString(@"Refreshed", @"HUD Complete");
                              //[hud hideAnimated:YES afterDelay:3.f];

                          }];
                          
                      }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}


/*- (void)loadHTMLString:(NSString *)string {
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSString *htmlString = [NSString stringWithFormat:@"<html><head><link href=\"default.css\" rel=\"stylesheet\" type=\"text/css\" /></head><body>%@</body></html>", string];
    
    [webView loadHTMLString:htmlString baseURL:baseURL];
    NSLog(@"%@", htmlString);
}*/


#pragma mark - UITableViewDelegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[MainTableViewCell description] forIndexPath:indexPath];
    
    cell.mainView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.mainView.layer.borderWidth = 1.0f;
    cell.mainView.layer.cornerRadius = 4.0f;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [cell.readImageView setHidden:true];
    
    if (self.kContentType == kContentTypeNews) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *title = [[self.articles objectAtIndex:indexPath.section] objectForKey:@"new_title"];
            cell.headlineLabel.text = title;
            
            if ([defaults boolForKey:title] == true) {
                //[cell.readImageView setHidden:false];
                cell.headlineLabel.textColor = [UIColor lightGrayColor];
                cell.nameLabel.textColor = [UIColor lightGrayColor];
            } else {
                cell.headlineLabel.textColor = [UIColor darkTextColor];
                cell.nameLabel.textColor = [UIColor colorWithRed:103.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0f];
            }
            
            NSString *dateString = [[self.articles objectAtIndex:indexPath.section] objectForKey:@"date"];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"LLLL d, yyyy"];
            NSDate *date = [formatter dateFromString:dateString];

            
            NSString *topic = [[self.articles objectAtIndex:indexPath.section] objectForKey:@"topic"];
            
            cell.nameLabel.text = [NSString stringWithFormat:@"%@ - %@ ago", topic, [date timeAgoSimple]];
            
            
            NSString *imageString = [[self.articles objectAtIndex:indexPath.section] objectForKey:@"image"];
            if (imageString == nil) {
                [cell.contentImageView hnk_setImageFromURL:[NSURL URLWithString:@"https://www.aha.org/sites/default/files/2018-03/AHANews_logo_2.jpg"]];
            } else {
                [cell.contentImageView hnk_setImageFromURL:[NSURL URLWithString:imageString]];
            }
        });
        
        return cell;
        
    } else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            VoterVoiceBody *body = (VoterVoiceBody *)self.voter.response.body[indexPath.section];
            cell.nameLabel.text = body.name;
            cell.headlineLabel.text = body.headline;
            [cell.contentImageView hnk_setImageFromURL:[NSURL URLWithString:body.imageUrl]];
        });
        
        /*cell.mainView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
         cell.mainView.layer.shadowOffset = CGSizeMake(0, 5);
         cell.mainView.layer.shadowOpacity = 1;
         cell.mainView.layer.shadowRadius = 1.0;*/
        
        return cell;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.kContentType == kContentTypeCampaigns) {
        VoterVoiceBody *body = (VoterVoiceBody *)self.voter.response.body[indexPath.section];
        CampaignInfoViewController *vc = (CampaignInfoViewController *)[self.storyboard instantiateViewControllerWithIdentifier:[CampaignInfoViewController description]];
        vc.body = body;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        //[self performSegueWithIdentifier:@"News" sender:nil];
        AHANewsViewController *vc = (AHANewsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:[AHANewsViewController description]];
        NSDictionary *articleDict = [self.articles objectAtIndex:indexPath.section];
        vc.articleDict = articleDict;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (self.kContentType == kContentTypeNews) {
        return self.articles.count;
    } else {
        return self.campaignList.count;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 220;
}

-(void)getCampaignSummaries {
    
    [self.action getCampaignSummaries:^(VoterVoice *voterVoice, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                self.campaignList = voterVoice.response.body;
                self.voter = voterVoice;
                [self.tableView reloadData];
                
            } else {
                NSLog(@"%@", error);
            }
        });
    }];
}

- (void)refreshFeed {
    [self.tableView reloadData];
}

- (void)getAHANews {
    
    NSURL *url = [NSURL URLWithString:@"https://www.aha.org/aha-news-rss-feed"];

    [self downloadDataFromURL:url withCompletionHandler:^(NSData *data) {
        
        if (data != nil) {
            self.xmlParser = [[NSXMLParser alloc] initWithData:data];
            self.xmlParser.delegate = self;
            
            // Initialize the mutable string that we'll use during parsing.
            self.foundValue = [[NSMutableString alloc] init];
            // Start parsing.
            [self.xmlParser parse];
        }
    }];
    
    [self.tableView reloadData];
}

-(void)parserDidStartDocument:(NSXMLParser *)parser{
    //Initialize the articles mutable array.
    self.articles = [[NSMutableArray alloc] init];
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    //When parsing is done, reload the tableView.
    [self.tableView reloadData];
}

-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    //Detect for errors in parsing the XML.
    NSLog(@"%@", [parseError localizedDescription]);
}


-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    // If the current element name is equal to "geoname" then initialize the temporary dictionary.
    if ([elementName isEqualToString:@"item"]) {
        self.dictTempDataStorage = [[NSMutableDictionary alloc] init];
    }
    
    // Keep the current element.
    self.currentElement = elementName;
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    // Store the found characters if only we're interested in the current element.
    if ([self.currentElement isEqualToString:@"title"] ||
        [self.currentElement isEqualToString:@"link"] ||
        [self.currentElement isEqualToString:@"description"]) {
        
        if (![string isEqualToString:@"\n"]) {
            [self.foundValue appendString:string];
        }
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if ([elementName isEqualToString:@"item"]) {
        [self.articles addObject:[[NSDictionary alloc] initWithDictionary:self.dictTempDataStorage]];
    }
    
    else if ([elementName isEqualToString:@"title"]){
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"title"];
    }
    else if ([elementName isEqualToString:@"link"]){
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"link"];
    }
    else if ([elementName isEqualToString:@"description"]){
        [self.dictTempDataStorage setObject:[NSString stringWithString:self.foundValue] forKey:@"description"];
        
        // Clear the mutable string.
        [self.foundValue setString:@""];
        
        //Now we parse the HTML string :(
        
        NSError *error;
        NSString *htmlString = [self.dictTempDataStorage objectForKey:@"description"];
        HTMLParser *parser = [[HTMLParser alloc] initWithString:htmlString error:&error];
        
        if (error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        HTMLNode *bodyNode = [parser body];
        
        NSArray *spanNodes = [bodyNode findChildTags:@"span"];
        
        for (HTMLNode *spanNode in spanNodes) {
            
            if ([[spanNode getAttributeNamed:@"class"] isEqualToString:@"title"]) {
            
                //Title
                 [self.dictTempDataStorage setObject:[NSString stringWithString:[self stripHTML:spanNode.rawContents]] forKey:@"new_title"];
            }
            
            if ([[spanNode getAttributeNamed:@"class"] isEqualToString:@"created"]) {
                
                [self.dictTempDataStorage setObject:[NSString stringWithString:[self stripHTML:spanNode.rawContents]] forKey:@"date"];
                
            }
        }

        NSArray *divNodes = [bodyNode findChildTags:@"div"];
        
        for (HTMLNode *divNode in divNodes) {
            
            if ([[divNode getAttributeNamed:@"class"] isEqualToString:@"field_media_featured_image"]) {
                
                NSArray *imageNodes = [bodyNode findChildTags:@"img"];
                
                for (HTMLNode *node in imageNodes) {

                    NSString *image = [node getAttributeNamed:@"src"];
                    
                    //Image
                    [self.dictTempDataStorage setObject:[NSString stringWithString:image] forKey:@"image"];
                    NSLog(@"%@", self.dictTempDataStorage);
                }
                
            } else if ([[divNode getAttributeNamed:@"class"] isEqualToString:@"field_topics"]) {
                
                NSArray *topicNodes = [bodyNode findChildTags:@"a"];

                for (HTMLNode *topicNode in topicNodes) {
                    
                    //topic
                    [self.dictTempDataStorage setObject:[NSString stringWithString:[topicNode allContents]] forKey:@"topic"];
                    
                    //url
                    [self.dictTempDataStorage setObject:[NSString stringWithString:[topicNode getAttributeNamed:@"href"]] forKey:@"new_link"];
                }
            } else if ([[divNode getAttributeNamed:@"class"] isEqualToString:@"body"]) {
                
                NSLog(@"%@", divNode);
                
                //description
                [self.dictTempDataStorage setObject:[NSString stringWithString:[divNode allContents]] forKey:@"new_description"];
            }
        }
    }
}


- (IBAction)newsButtonPressed:(id)sender {
    self.kContentType = kContentTypeNews;
    
    //[self.newsButton setTintColor:kAHABlue];
    //[self.campaignsButton setTintColor:[UIColor darkTextColor]];
    
    [self underline:self.newsButton withColor:kAHABlue];
    [self underline:self.campaignsButton withColor:[UIColor groupTableViewBackgroundColor]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getAHANews];
    });
}

- (IBAction)campaignsButtonPressed:(id)sender {
    
    self.kContentType = kContentTypeCampaigns;
    
    //[self.campaignsButton setTintColor:kAHABlue];
    //[self.newsButton setTintColor:[UIColor darkTextColor]];
    
    [self underline:self.campaignsButton withColor:kAHABlue];
    [self underline:self.newsButton withColor:[UIColor groupTableViewBackgroundColor]];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

//changing to UIView from UILabel
-(void)underline: (UIButton *)button withColor:(UIColor *)color
{
    CGRect frame = button.frame;
    CALayer *bottomLayer = [[CALayer alloc] init];
    bottomLayer.frame = CGRectMake(0, frame.size.height - 1, frame.size.width, 1.0);
    bottomLayer.backgroundColor = color.CGColor;
    
    [button.layer addSublayer:bottomLayer];
}


- (void)downloadDataFromURL:(NSURL *)url withCompletionHandler:(void (^)(NSData *))completionHandler {
    // Instantiate a session configuration object.
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    // Instantiate a session object.
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    // Create a data task object to perform the data downloading.
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error != nil) {
            // If any error occurs then just display its description on the console.
            NSLog(@"%@", [error localizedDescription]);
        }
        else{
            // If no error occurs, check the HTTP status code.
            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
            
            // If it's other than 200, then show it on the console.
            if (HTTPStatusCode != 200) {
                NSLog(@"HTTP status code = %ld", (long)HTTPStatusCode);
            }
            
            // Call the completion handler with the returned data on the main thread.
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionHandler(data);
            }];
        }
    }];
    
    // Resume the task.
    [task resume];
}

-(NSString *) stripHTML:(NSString*)originalString {
    
    NSRange range;
    
    NSString *string = [originalString copy];
    
    while ((range = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        string = [string stringByReplacingCharactersInRange:range withString:@""];
    
    return string;
}

@end
