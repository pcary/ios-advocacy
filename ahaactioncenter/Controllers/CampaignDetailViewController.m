//
//  CampaignDetailViewController.m
//  ahaactioncenter
//
//  Created by Vince Davis on 4/6/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import "CampaignDetailViewController.h"
#import "ProgressHUD.h"
#import "MBProgressHUD.h"
#import "ActionCenterManager.h"
#import "FontAwesomeKit.h"
#import "Constants.h"
#import "CampaignDetailView.h"
#import "KGModal.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "MenuViewController.h"
#import "UpdateUserViewController.h"
#import "NewMessageTableViewCell.h"
#import "ahaactioncenter-Swift.h"
#import <Haneke/Haneke.h>

@interface CampaignDetailViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>
{
    ProgressHUD *hud;
    ActionCenterManager *action;
    BOOL editable;
    NSString *guidelines;
    NSString *subject;
    NSString *message;
    VoterVoice *voterVoice;
    VoterVoice *matchVoter;
    BOOL campaignFlow;
    NSMutableSet *required;
    UIImage *profileImage;
}

@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
//@property (weak, nonatomic) IBOutlet UITextView *message;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UITextField *subjectTextField;

@end

@implementation CampaignDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"New Message";
    editable = NO;
    
    required = [[NSMutableSet alloc] init];
    self.tableView.estimatedRowHeight = 60.0;
    
    self.messageLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.messageLabel.layer.borderWidth = 0.5f;
    
    self.subjectTextField.layer.cornerRadius = 4.0f;
    self.subjectTextField.layer.borderWidth = 1.0f;
    self.subjectTextField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.subjectTextField.layer.masksToBounds = YES;
    
    hud = [ProgressHUD sharedInstance];
    action = [ActionCenterManager sharedInstance];
    
    campaignFlow = YES;
    [self setupCampaignFlow];
    [self showGuidelines:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if (_campaignID == nil) {
        campaignFlow = NO;
        [self setupContactUsFlow];
        self.navigationItem.leftBarButtonItem = [ActionCenterManager dragButton];
    }
}

- (void)setupCampaignFlow {

    NSLog(@"%@", self.bodies);
    
    FAKIonIcons *icon = [FAKIonIcons iconWithCode:@"\uf44c" size:30];
    [icon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[icon imageWithSize:CGSizeMake(30, 30)] style:UIBarButtonItemStyleDone target:self action:@selector(showGuidelines:)];
    
   // [hud showHUDWithMessage:@"Getting Message"];
    
    [action getTargetedMessages:_campaignID completion:^(VoterVoice *voter, NSError *error) {
        
        if (!error) {
            NSInteger indexOfTheMessage = [self indexOfTheMessage];
            
           // [hud showHUDSucces:YES withMessage:@"Completed"];
            
            VoterVoiceBody *body = voter.response.body[indexOfTheMessage];
            VoterVoiceMessage *vvMessage = body.messages[0];
            
            if ([body.phoneCallMethod isKindOfClass:[NSNull class]]) {
                body.phoneCallMethod = @"";
            }
            if ([vvMessage.phoneGuidelines isKindOfClass:[NSNull class]]) {
                vvMessage.phoneGuidelines = @"";
            }
            
            subject = vvMessage.subject;
            message = vvMessage.message;
            guidelines = vvMessage.guidelines;
            voterVoice = voter;
        }
        
        [self.tableView reloadData];
    }];
}

- (NSInteger)indexOfTheMessage
{
    NSInteger indexOfTheMessage;
    
    if ([self.groupID rangeOfString:@"not"].location == NSNotFound) {

        if ([self.groupID rangeOfString:@"Co-sponsors"].location == NSNotFound){

            // This person sponsored the bill
            indexOfTheMessage = 1;
            VoterVoiceBody *body = (VoterVoiceBody *)voterVoice.response.body[1];
            NSLog(@"body id %@", body.id);
            
        } else {
            // This person co-sponsored the bill
            indexOfTheMessage = 0;
            VoterVoiceBody *body = (VoterVoiceBody *)voterVoice.response.body[0];
            NSLog(@"body id %@", body.id);
        }

    } else {
        // This person did not sponsor or co-sponsor the bill
        indexOfTheMessage = 2;
        VoterVoiceBody *body = (VoterVoiceBody *)voterVoice.response.body[2];
        NSLog(@"body id %@", body.id);
    }
    
    return indexOfTheMessage;
}

- (void)setupContactUsFlow {
    self.title = @"Contact AHA";
    self.subjectTextField.text = @"Send a Message to AHA";
    
    //_message.inputAccessoryView = [self dismissBar];
}

- (void)textViewDidChange:(UITextView *)textView {
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        NewMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ToCell" forIndexPath:indexPath];
        
        cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.size.height / 2;
        
        [action getProfile:[self.currentMatch.id stringValue]
                  withType:self.currentMatch.type
                completion:^(NSDictionary *dict, NSError *error) {
                                        
                    NSString *imageString = [dict valueForKeyPath:@"response.body.photoUrl"];
                    NSURL *imageUrl = [NSURL URLWithString:imageString];
                    [cell.profileImageView hnk_setImageFromURL:imageUrl];
                    
                    NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
                    profileImage = [UIImage imageWithData:imageData];
                    
                }];

        cell.nameLabel.text = self.currentMatch.name;
        
        return cell;
        
    } else if (indexPath.section == 1) {
        
        NewMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubjectCell" forIndexPath:indexPath];
        
        cell.subjectTextView.tag = 1;
        
        if (editable == YES) {
            [self textViewDidBeginEditing:cell.subjectTextView];
            
            [cell.subjectTextView performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0];
            
        } else if (editable == NO) {
            [self textViewDidEndEditing:cell.subjectTextView];
        }
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:cell.subjectTextView.text];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 8;
        
        NSDictionary *dict = @{NSParagraphStyleAttributeName:paragraphStyle};
        [attributedString addAttributes:dict range:NSMakeRange(0, [cell.subjectTextView.text length])];
        
        cell.subjectTextView.attributedText = attributedString;
        [cell.subjectTextView setFont:[UIFont fontWithName:@"Helvetica" size:17]];
        cell.subjectTextView.adjustsFontForContentSizeCategory = NO;
        
        return cell;
    }
    else {
        NewMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
        
        cell.messageTextView.tag = 2;
        
        if (editable == YES) {
            [self textViewDidBeginEditing:cell.messageTextView];
            
        } else if (editable == NO) {
            [self textViewDidEndEditing:cell.messageTextView];
        }
        return cell;
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    
    textView.selectable = YES;
    textView.editable = YES;
    
    self.navigationItem.rightBarButtonItem.title = @"Done";
    self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStyleDone;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    
    textView.selectable = NO;
    textView.editable = NO;
    
    self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem.title = @"Edit";
    [textView performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    
    if (textView.tag == 1) {
        subject = textView.text;
    } else {
        message = textView.text;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (void)showGuidelines:(id)sender {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [action getMatchesForCampaign:_campaignID
                        withToken:[prefs objectForKey:@"token"]
                       completion:^(VoterVoice *voter, NSError *err) {
                           
                           if (!err && [voter.response.status intValue] == 200) {
                               
                               if (voter.response.body != nil) {
                                  
                               UIView *header;
                               header = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aha_logo_campaign"]];
                               
                               header.contentMode = UIViewContentModeBottom;
                               header.clipsToBounds = YES;
                               header.frame = CGRectMake(0, 0, 0, 50);
                               
                               CFAlertViewController *alertController = [CFAlertViewController alertControllerWithTitle:@"Message Guidelines" titleColor:kAHABlue message:[self guidelinesMessage] messageColor:[UIColor blackColor] textAlignment:NSTextAlignmentCenter preferredStyle:CFAlertControllerStyleAlert headerView:header footerView:nil didDismissAlertHandler:^(BOOL dismiss) {
                                   
                                   dismiss = YES;
                               }];
                                   
                                CFAlertAction *alert = [CFAlertAction actionWithTitle:@"GO TO MESSAGE" style:CFAlertActionStyleDefault alignment:CFAlertActionAlignmentJustified backgroundColor:kAHARed textColor:[UIColor whiteColor] handler:^(CFAlertAction *alert) {
                                   
                               }];
                               
                               [alertController addAction:alert];
                               [self presentViewController:alertController animated:YES completion:nil];
                               }
                           }
                       }];
}

- (NSString *)getTargetString:(VoterVoiceMatches *)m atIndex:(int)i {
    
    NSMutableString *targetString = [NSMutableString new];
    
    [targetString appendString:[NSString stringWithFormat:@"&targets[%d][type]=%@", i, m.type]];
    [targetString appendString:[NSString stringWithFormat:@"&targets[%d][id]=%@", i, [m.id stringValue]]];
    [targetString appendString:[NSString stringWithFormat:@"&targets[%d][deliveryMethod]=webform", i]];
    
    BOOL prefixIncluded = NO;
    VoterVoiceBody *body = (VoterVoiceBody *)voterVoice.response.body[0];
    for (int j = 0; j < body.preSelectedAnswers.count; j++) {
        VoterVoiceSelectedAnswers *answers = (VoterVoiceSelectedAnswers *)body.preSelectedAnswers[j];
        if ([answers.question isEqualToString:@"Prefix"]) {
            prefixIncluded = YES;
        }
        [targetString appendString:[NSString stringWithFormat:@"&targets[%d][questionnaire][%d][question]=%@",
                                    i,
                                    j,
                                    answers.question]];
        [targetString appendString:[NSString stringWithFormat:@"&targets[%d][questionnaire][%d][answer]=%@",
                                    i,
                                    j,
                                    answers.answer]];
    }
    
    if (!prefixIncluded) {
        // add Mr
        [targetString appendString:[NSString stringWithFormat:@"&targets[%d][questionnaire][%d][question]=%@",
                                    i,
                                    (int)body.preSelectedAnswers.count,
                                    @"Prefix"]];
        [targetString appendString:[NSString stringWithFormat:@"&targets[%d][questionnaire][%d][answer]=%@",
                                    i,
                                    (int)body.preSelectedAnswers.count,
                                    @"Mr."]];
    }
    
    return targetString;
}

- (void)sendContactUsMessage {
    NSString *messageText, *from;
    
    if (![action isReachable]) {
        [self showNoInternet];
        return;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    OAM *oam = [[OAM alloc] initWithJSONData:[prefs dataForKey:@"user"]];
    messageText = [[NSString stringWithFormat:@"%@\n\nSent from %@ %@\n%@", message, oam.first_name, oam.last_name, oam.org_name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    from = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://54.245.255.190/p/action_center/api/v1/sendEmail?message=%@&from=%@", messageText, from]];
    
        [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            
            if (error) {
                // NSLog(@"Error %@", error);
            } else {
                [self showThankYou];
            }
        }];
}

- (void)showThankYou {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success"
                                                                   message:@"Your message has been sent. Thank you for contacting the American Hospital Association"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Done"
                                              style:UIAlertActionStyleDefault
                                            handler:^void (UIAlertAction *action)
                      {
                          //[self.navigationController popToRootViewControllerAnimated:YES];
                          AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                          //UIStoryboard *sb = [[ad.window rootViewController] storyboard];
                          MainViewController *main = (MainViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"main"];
                          UINavigationController *mainNav = [[UINavigationController alloc] initWithRootViewController:main];
                          mainNav.toolbarHidden = NO;
                          MenuViewController *menu = (MenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"menu"];
                          UINavigationController *menuNav = [[UINavigationController alloc] initWithRootViewController:menu];
                          menuNav.toolbarHidden = NO;
                          if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
                              AppDelegate *ad = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                              UISplitViewController *split = (UISplitViewController *)ad.splitViewController;
                              [split setViewControllers:@[menuNav, mainNav]];
                              //NSLog(@"ipad");
                          }
                          else {
                              //NSLog(@"iphone");
                              
                              MSDynamicsDrawerViewController *dynamic = (MSDynamicsDrawerViewController *)ad.dynamicsDrawerViewController;
                              [dynamic setPaneState:MSDynamicsDrawerPaneStateClosed];
                              [dynamic setPaneViewController:mainNav animated:YES completion:nil];
                          }

                      }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showNoInternet {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Internet"
                                                                   message:@"Please check your internet connection and try again"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleDefault
                                            handler:^void (UIAlertAction *action)
                      {
                          
                      }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)sendMessage:(id)sender {
    if (campaignFlow) {
        [self sendCampaignMessage];
    }
    else {
        [self sendContactUsMessage];
    }
}

- (void)showAlert:(NSString *)alert withMessage:(NSString *)msg {
    UIAlertController *alert2 = [UIAlertController alertControllerWithTitle:alert
                                                                    message:msg
                                                             preferredStyle:UIAlertControllerStyleAlert];
    [alert2 addAction:[UIAlertAction actionWithTitle:@"Update Info"
                                               style:UIAlertActionStyleCancel
                                             handler:^void (UIAlertAction *action)
                       {
                           UpdateUserViewController *update = [[UpdateUserViewController alloc] initWithStyle:UITableViewStyleGrouped];
                           update.excludeList = @[@"excludedList", @"showPhone"/*, @"firstName", @"lastName", @"prefix", @"phone"*/];
                           UserForm *form = (UserForm *)update.formController.form;
                           form.firstName = @"test";
                           update.validateAddress = YES;
                           update.showCancel = YES;
                           UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:update];
                           nav.modalPresentationStyle = UIModalPresentationFormSheet;
                           [self.navigationController presentViewController:nav animated:YES completion:nil];
                       }]];
    [alert2 addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                               style:UIAlertActionStyleDefault
                                             handler:^void (UIAlertAction *action)
                       {
                           
                       }]];
    [self presentViewController:alert2 animated:YES completion:nil];
}

- (void)sendCampaignMessage {
    // Create the array of matched targets to be sent in the GET request
    
    NSMutableString *targetString = [NSMutableString new];
    
    [targetString appendString:[self getTargetString:self.currentMatch atIndex:0]];
    
    //[self viewFadeOut:background];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *prefix = [prefs stringForKey:@"prefix"];
    NSString *phone = [prefs stringForKey:@"phone"];
    
//    NSString *address = [prefs stringForKey:@"address"];
//    NSString *city = [prefs stringForKey:@"city"];
//    NSString *state = [prefs stringForKey:@"state"];
//    NSString *zip = [prefs stringForKey:@"zip"];
    
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    if ([required containsObject:@"phone"] && phone.length != 10) {
        [self showAlert:@"Phone number needed" withMessage:@"One of the legislators needs your phone number to send a message. Please update your info with your phone number."];
        return;
    }
    
    if ([required containsObject:@"prefix"] && prefix.length < 1) {
        [self showAlert:@"Prefix needed" withMessage:@"One of the legislators needs your prefix to send a message. Please update your info with your prefix."];
        return;
    }

    //OAM *oam = [[OAM alloc] initWithJSONData:[prefs dataForKey:@"user"]];
 
    //Until login is figured out 8.20.18
    NSString *firstName = [prefs objectForKey:@"firstName"];
    NSString *lastName = [prefs objectForKey:@"lastName"];
    NSString *address = [prefs objectForKey:@"address"];
    NSString *zip = [prefs objectForKey:@"zip"];

    NSInteger indexOfTheBody = [self indexOfTheMessage];
    VoterVoiceBody *body = (VoterVoiceBody *)voterVoice.response.body[indexOfTheBody];
    
    //VoterVoiceMessage *message = (VoterVoiceMessage *)body.messages[0];
    
    NSMutableString *urlString = [NSMutableString new];
    [urlString appendString:@"http://54.245.255.190/p/action_center/api/v1/postResponse?"];
    [urlString appendString:[NSString stringWithFormat:
                            @"token=%@"
                             "&subject=%@"
                             "&body=%@"
                             "&userId=%@"
                             "&signature=%@"
                             "&messageId=%@"
                             "%@"
                             "&phone=%@"
                             "&email=%@"
                             "&address=%@"
                             "&zipcode=%@"
                             "&country=US"
                             "&campaignId=%@",
                             [prefs stringForKey:@"token"],self.subjectTextField.text,message,[prefs stringForKey:@"userId"],[NSString stringWithFormat:@"%@ %@", firstName, lastName],[body.id stringValue],targetString,([required containsObject:@"phone"]) ? phone : @"",[prefs stringForKey:@"email"],address,[zip substringToIndex:5],self.campaignID
                             ]
     ];
    
    if (![required containsObject:@"phone"]) {
        urlString = (NSMutableString *)[urlString stringByReplacingOccurrencesOfString:@"&phone=" withString:@""];
    }
    
    NSLog(@"\n\nURL STRING:\n\n%@", [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    //NSURL *url =[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    [hud showHUDWithMessage:@"Sending"];
    //MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //hud.label.text = NSLocalizedString(@"Sending...", @"HUD Sending");
    
    //NSLog(@"\n\nURL STRING:\n\n%@", urlString);
    
    [action postVoterUrl:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
              completion:^(VoterVoice *voter, NSError *err) {
                  if (!err) {
                      NSLog(@"Status %@  Response: %@", [voter.response.status stringValue], voter.response.body);
                      
                      [hud showHUDSucces:YES withMessage:@"Sent"];
                      //hud.label.text = NSLocalizedString(@"Sent", @"HUD Sent");
                      //[hud hideAnimated:YES afterDelay:3.f];
                      
                      [self showCompleted];
                  }
              }];
     
}

- (void)showCompleted {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Email Sent"
                                                                   message:@"You will receive a confirmation email shortly. Thank you"
                                                            preferredStyle:UIAlertControllerStyleAlert];

    [alert addAction:[UIAlertAction actionWithTitle:@"Done"
                                              style:UIAlertActionStyleDefault
                                            handler:^void (UIAlertAction *action)
                      {
                          [self.navigationController popToRootViewControllerAnimated:YES];
                          
                      }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)editTapped:(id)sender {
    if (editable == NO) {
        editable = YES;
        [self.tableView reloadData];
    } else if (editable == YES) {
        editable = NO;
        [self.tableView reloadData];
    }
}

- (IBAction)cancelTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(NSString *)guidelinesMessage {
    
    NSString *url = [NSString stringWithFormat:@"http://54.245.255.190/p/action_center/api/v1/messagedeliveryoptions?targettype=E&targetid=%@", self.currentMatch.id];
    
    NSURL *requireURL = [NSURL URLWithString:url];
    NSData *jsonData = [NSData dataWithContentsOfURL:requireURL];
    NSError *error = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    
    NSArray *arr = (NSArray *)[dict valueForKeyPath:@"response.body"];
    
    NSMutableString *requiredString = [NSMutableString new];

    for (NSDictionary *d in arr) {
        
        if ([d[@"deliveryMethod"] isEqualToString:@"webform"]) {
            NSArray *array = (NSArray *)d[@"requiredUserFields"];
            
            //[requiredString appendString:@"Required Fields: "];
            //[requiredString appendString:@"\n"];
            
            for (NSString *s in array) {

                //NSString *string  = [NSString stringWithFormat:@"%@ ",[s capitalizedString]];
                //[requiredString appendString:string];
                //[requiredString appendString:@"\n"];
            }
            NSLog(@"%@", requiredString);
        }
    }
    NSString *messageString1 = @"Please consider adding the following to personalize your message:";
    NSString *messageString2 = @"• An opening paragraph introducing yourself and your hospital.";
    NSString *messageString3 = @"• The importance of this legislation to your hospital and the community.";
    
    NSString *messageString4 = [NSString stringWithFormat:@"%@\n\n%@\n\n%@", messageString1, messageString2, messageString3];
    
    return messageString4;
}

@end
