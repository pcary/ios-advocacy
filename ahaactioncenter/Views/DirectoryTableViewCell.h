//
//  DirectoryTableViewCell.h
//  AHA Advocacy
//
//  Created by Tibble, Patrick on 2/13/15.
//  Copyright (c) 2015 AHA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Haneke/Haneke.h>

@interface DirectoryTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *directoryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *messageImageView;

@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
